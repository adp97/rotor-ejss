/* _inputParameters: an object with different values for the model parameters */
function tfg10(_topFrame,_libraryPath,_codebasePath, _inputParameters) {
  var _model = EJSS_CORE.createAnimationLMS();
  var _view;
  var _isPlaying = false;
  var _isPaused = true;
  var _isMobile = (navigator===undefined) ? false : navigator.userAgent.match(/iPhone|iPad|iPod|Android|BlackBerry|Opera Mini|IEMobile/i);

var _stringProperties = {};
  var _tools = {
    showInputDialog : EJSS_INTERFACE.BoxPanel.showInputDialog,
    showOkDialog : EJSS_INTERFACE.BoxPanel.showOkDialog,
    showOkCancelDialog : EJSS_INTERFACE.BoxPanel.showOkCancelDialog,
    downloadText: EJSS_TOOLS.File.downloadText,
    uploadText: function(action) { EJSS_TOOLS.File.uploadText(_model,action); } 
  };

  function _play()  { _isPaused = false; _isPlaying = true;  _model.play();  }
  function _pause() { _isPaused = true;  _isPlaying = false; _model.pause(); }
  function _step()  { _pause();  _model.step(); }
  function _reset() { _model.reset();  _isPaused = _model.isPaused(); _isPlaying = _model.isPlaying(); }
  _model._play  = _play;
  _model._pause = _pause;
  _model._step  = _step;
  _model._reset = _reset;
  function _update() { _model.update(); }
  function _initialize() { _model.initialize(); }
  function _setFPS(_fps) { _model.setFPS(_fps); }
  function _setDelay(_delay) { _model.setDelay(_delay); }
  function _setStepsPerDisplay(_spd) { _model.setStepsPerDisplay(_spd); }
  function _setUpdateView(_updateView) { _model.setUpdateView(_updateView); }
  function _setAutoplay(_auto) { _model.setAutoplay(_auto); }
  function _println(_message) { console.log(_message); }

  function _breakAfterThisPage() { _model.setShouldBreak(true); }

  function _resetSolvers() { if (_model.resetSolvers) _model.resetSolvers(); }

  function _saveText(name,type,content) { if (_model.saveText) _model.saveText(name,type,content); }

  function _saveState(name) { if (_model.saveState) _model.saveState(name); }

  function _saveImage(name,panelname) { if (_model.saveImage) _model.saveImage(name,panelname); }

  function _readState(url,type) { if (_model.readState) _model.readState(url,type); }

  function _readText(url,type,varname) { if (_model.readText) _model.readText(url,type,varname); }

  function _getStringProperty(propertyName) {
    var _value = _stringProperties[propertyName];
    if (_value===undefined) return propertyName;
    else return _value;
  }
  var __pagesEnabled = [];
  function _setPageEnabled(pageName,enabled) { __pagesEnabled[pageName] = enabled; }

  var theta0; // EjsS Model.Variables.Constants.theta0
  var z0; // EjsS Model.Variables.Constants.z0
  var g; // EjsS Model.Variables.Constants.g
  var pi; // EjsS Model.Variables.Constants.pi
  var rho; // EjsS Model.Variables.Constants.rho

  var t; // EjsS Model.Variables.Dynamic.t
  var dt; // EjsS Model.Variables.Dynamic.dt
  var Kp; // EjsS Model.Variables.Dynamic.Kp
  var Ki; // EjsS Model.Variables.Dynamic.Ki
  var Kd; // EjsS Model.Variables.Dynamic.Kd
  var errorP; // EjsS Model.Variables.Dynamic.errorP
  var errorI; // EjsS Model.Variables.Dynamic.errorI
  var P; // EjsS Model.Variables.Dynamic.P
  var I; // EjsS Model.Variables.Dynamic.I
  var D; // EjsS Model.Variables.Dynamic.D
  var theta; // EjsS Model.Variables.Dynamic.theta
  var z; // EjsS Model.Variables.Dynamic.z
  var vT; // EjsS Model.Variables.Dynamic.vT
  var zT; // EjsS Model.Variables.Dynamic.zT
  var zMax; // EjsS Model.Variables.Dynamic.zMax
  var zMin; // EjsS Model.Variables.Dynamic.zMin
  var PM; // EjsS Model.Variables.Dynamic.PM
  var vG; // EjsS Model.Variables.Dynamic.vG

  var zSP; // EjsS Model.Variables.Parametric.zSP
  var mMotor; // EjsS Model.Variables.Parametric.mMotor
  var mBar; // EjsS Model.Variables.Parametric.mBar
  var mPropeller; // EjsS Model.Variables.Parametric.mPropeller
  var m; // EjsS Model.Variables.Parametric.m
  var N; // EjsS Model.Variables.Parametric.N
  var diameterPropeller; // EjsS Model.Variables.Parametric.diameterPropeller
  var r; // EjsS Model.Variables.Parametric.r
  var Nblades; // EjsS Model.Variables.Parametric.Nblades
  var KV; // EjsS Model.Variables.Parametric.KV
  var i; // EjsS Model.Variables.Parametric.i
  var R; // EjsS Model.Variables.Parametric.R
  var U; // EjsS Model.Variables.Parametric.U
  var w; // EjsS Model.Variables.Parametric.w
  var L; // EjsS Model.Variables.Parametric.L
  var kv; // EjsS Model.Variables.Parametric.kv
  var ke; // EjsS Model.Variables.Parametric.ke
  var th; // EjsS Model.Variables.Parametric.th
  var t_L; // EjsS Model.Variables.Parametric.t_L
  var t_E; // EjsS Model.Variables.Parametric.t_E
  var J; // EjsS Model.Variables.Parametric.J
  var IMax; // EjsS Model.Variables.Parametric.IMax
  var Vin; // EjsS Model.Variables.Parametric.Vin
  var Vmax; // EjsS Model.Variables.Parametric.Vmax
  var Vmin; // EjsS Model.Variables.Parametric.Vmin
  var Ptest; // EjsS Model.Variables.Parametric.Ptest
  var Etest; // EjsS Model.Variables.Parametric.Etest
  var w0; // EjsS Model.Variables.Parametric.w0
  var I0; // EjsS Model.Variables.Parametric.I0
  var k2; // EjsS Model.Variables.Parametric.k2
  var b; // EjsS Model.Variables.Parametric.b

  var height; // EjsS Model.Variables.UI.height
  var width; // EjsS Model.Variables.UI.width
  var heightGraph; // EjsS Model.Variables.UI.heightGraph
  var widthGraph; // EjsS Model.Variables.UI.widthGraph
  var heightGraph2; // EjsS Model.Variables.UI.heightGraph2
  var widthGraph2; // EjsS Model.Variables.UI.widthGraph2
  var width3D; // EjsS Model.Variables.UI.width3D
  var height3D; // EjsS Model.Variables.UI.height3D
  var DpropellerView; // EjsS Model.Variables.UI.DpropellerView
  var TpropellerView; // EjsS Model.Variables.UI.TpropellerView
  var DpropellerSelector; // EjsS Model.Variables.UI.DpropellerSelector
  var TpropellerSelector; // EjsS Model.Variables.UI.TpropellerSelector
  var zOffset; // EjsS Model.Variables.UI.zOffset
  var cameraAltitude; // EjsS Model.Variables.UI.cameraAltitude
  var cameraAzimuth; // EjsS Model.Variables.UI.cameraAzimuth
  var cameraX; // EjsS Model.Variables.UI.cameraX
  var cameraY; // EjsS Model.Variables.UI.cameraY
  var cameraZ; // EjsS Model.Variables.UI.cameraZ
  var cameraInitial; // EjsS Model.Variables.UI.cameraInitial
  var cameraAbove; // EjsS Model.Variables.UI.cameraAbove
  var cameraProjection; // EjsS Model.Variables.UI.cameraProjection
  var cameraTilt; // EjsS Model.Variables.UI.cameraTilt
  var cameraNear; // EjsS Model.Variables.UI.cameraNear
  var cameraFar; // EjsS Model.Variables.UI.cameraFar
  var cFX; // EjsS Model.Variables.UI.cFX
  var cFY; // EjsS Model.Variables.UI.cFY
  var cFZ; // EjsS Model.Variables.UI.cFZ
  var acrylicTransparency; // EjsS Model.Variables.UI.acrylicTransparency
  var vectorsView; // EjsS Model.Variables.UI.vectorsView
  var taux; // EjsS Model.Variables.UI.taux

  var _privateOdesList;
  var _ODEi_evolution1;
  var userEvents1=[];

  _model.getOdes = function() { return [_ODEi_evolution1]; };

  _model.removeEvents = function(){
    userEvents1=[];
  };

  function _serialize() { return _model.serialize(); }

  _model._userSerialize = function() {
    return {
      theta0 : theta0,
      z0 : z0,
      g : g,
      pi : pi,
      rho : rho,
      t : t,
      dt : dt,
      Kp : Kp,
      Ki : Ki,
      Kd : Kd,
      errorP : errorP,
      errorI : errorI,
      P : P,
      I : I,
      D : D,
      theta : theta,
      z : z,
      vT : vT,
      zT : zT,
      zMax : zMax,
      zMin : zMin,
      PM : PM,
      vG : vG,
      zSP : zSP,
      mMotor : mMotor,
      mBar : mBar,
      mPropeller : mPropeller,
      m : m,
      N : N,
      diameterPropeller : diameterPropeller,
      r : r,
      Nblades : Nblades,
      KV : KV,
      i : i,
      R : R,
      U : U,
      w : w,
      L : L,
      kv : kv,
      ke : ke,
      th : th,
      t_L : t_L,
      t_E : t_E,
      J : J,
      IMax : IMax,
      Vin : Vin,
      Vmax : Vmax,
      Vmin : Vmin,
      Ptest : Ptest,
      Etest : Etest,
      w0 : w0,
      I0 : I0,
      k2 : k2,
      b : b,
      height : height,
      width : width,
      heightGraph : heightGraph,
      widthGraph : widthGraph,
      heightGraph2 : heightGraph2,
      widthGraph2 : widthGraph2,
      width3D : width3D,
      height3D : height3D,
      DpropellerView : DpropellerView,
      TpropellerView : TpropellerView,
      DpropellerSelector : DpropellerSelector,
      TpropellerSelector : TpropellerSelector,
      zOffset : zOffset,
      cameraAltitude : cameraAltitude,
      cameraAzimuth : cameraAzimuth,
      cameraX : cameraX,
      cameraY : cameraY,
      cameraZ : cameraZ,
      cameraInitial : cameraInitial,
      cameraAbove : cameraAbove,
      cameraProjection : cameraProjection,
      cameraTilt : cameraTilt,
      cameraNear : cameraNear,
      cameraFar : cameraFar,
      cFX : cFX,
      cFY : cFY,
      cFZ : cFZ,
      acrylicTransparency : acrylicTransparency,
      vectorsView : vectorsView,
      taux : taux
    };
  };

  function _serializePublic() { return _model.serializePublic(); }

  _model._userSerializePublic = function() {
    return {
      theta0 : theta0,
      z0 : z0,
      g : g,
      pi : pi,
      rho : rho,
      t : t,
      dt : dt,
      Kp : Kp,
      Ki : Ki,
      Kd : Kd,
      errorP : errorP,
      errorI : errorI,
      P : P,
      I : I,
      D : D,
      theta : theta,
      z : z,
      vT : vT,
      zT : zT,
      zMax : zMax,
      zMin : zMin,
      PM : PM,
      vG : vG,
      zSP : zSP,
      mMotor : mMotor,
      mBar : mBar,
      mPropeller : mPropeller,
      m : m,
      N : N,
      diameterPropeller : diameterPropeller,
      r : r,
      Nblades : Nblades,
      KV : KV,
      i : i,
      R : R,
      U : U,
      w : w,
      L : L,
      kv : kv,
      ke : ke,
      th : th,
      t_L : t_L,
      t_E : t_E,
      J : J,
      IMax : IMax,
      Vin : Vin,
      Vmax : Vmax,
      Vmin : Vmin,
      Ptest : Ptest,
      Etest : Etest,
      w0 : w0,
      I0 : I0,
      k2 : k2,
      b : b,
      height : height,
      width : width,
      heightGraph : heightGraph,
      widthGraph : widthGraph,
      heightGraph2 : heightGraph2,
      widthGraph2 : widthGraph2,
      width3D : width3D,
      height3D : height3D,
      DpropellerView : DpropellerView,
      TpropellerView : TpropellerView,
      DpropellerSelector : DpropellerSelector,
      TpropellerSelector : TpropellerSelector,
      zOffset : zOffset,
      cameraAltitude : cameraAltitude,
      cameraAzimuth : cameraAzimuth,
      cameraX : cameraX,
      cameraY : cameraY,
      cameraZ : cameraZ,
      cameraInitial : cameraInitial,
      cameraAbove : cameraAbove,
      cameraProjection : cameraProjection,
      cameraTilt : cameraTilt,
      cameraNear : cameraNear,
      cameraFar : cameraFar,
      cFX : cFX,
      cFY : cFY,
      cFZ : cFZ,
      acrylicTransparency : acrylicTransparency,
      vectorsView : vectorsView,
      taux : taux
    };
  };

  _model._readParameters = function(json) {
    if(typeof json.theta0 != "undefined") theta0 = json.theta0;
    if(typeof json.z0 != "undefined") z0 = json.z0;
    if(typeof json.g != "undefined") g = json.g;
    if(typeof json.pi != "undefined") pi = json.pi;
    if(typeof json.rho != "undefined") rho = json.rho;
    if(typeof json.t != "undefined") t = json.t;
    if(typeof json.dt != "undefined") dt = json.dt;
    if(typeof json.Kp != "undefined") Kp = json.Kp;
    if(typeof json.Ki != "undefined") Ki = json.Ki;
    if(typeof json.Kd != "undefined") Kd = json.Kd;
    if(typeof json.errorP != "undefined") errorP = json.errorP;
    if(typeof json.errorI != "undefined") errorI = json.errorI;
    if(typeof json.P != "undefined") P = json.P;
    if(typeof json.I != "undefined") I = json.I;
    if(typeof json.D != "undefined") D = json.D;
    if(typeof json.theta != "undefined") theta = json.theta;
    if(typeof json.z != "undefined") z = json.z;
    if(typeof json.vT != "undefined") vT = json.vT;
    if(typeof json.zT != "undefined") zT = json.zT;
    if(typeof json.zMax != "undefined") zMax = json.zMax;
    if(typeof json.zMin != "undefined") zMin = json.zMin;
    if(typeof json.PM != "undefined") PM = json.PM;
    if(typeof json.vG != "undefined") vG = json.vG;
    if(typeof json.zSP != "undefined") zSP = json.zSP;
    if(typeof json.mMotor != "undefined") mMotor = json.mMotor;
    if(typeof json.mBar != "undefined") mBar = json.mBar;
    if(typeof json.mPropeller != "undefined") mPropeller = json.mPropeller;
    if(typeof json.m != "undefined") m = json.m;
    if(typeof json.N != "undefined") N = json.N;
    if(typeof json.diameterPropeller != "undefined") diameterPropeller = json.diameterPropeller;
    if(typeof json.r != "undefined") r = json.r;
    if(typeof json.Nblades != "undefined") Nblades = json.Nblades;
    if(typeof json.KV != "undefined") KV = json.KV;
    if(typeof json.i != "undefined") i = json.i;
    if(typeof json.R != "undefined") R = json.R;
    if(typeof json.U != "undefined") U = json.U;
    if(typeof json.w != "undefined") w = json.w;
    if(typeof json.L != "undefined") L = json.L;
    if(typeof json.kv != "undefined") kv = json.kv;
    if(typeof json.ke != "undefined") ke = json.ke;
    if(typeof json.th != "undefined") th = json.th;
    if(typeof json.t_L != "undefined") t_L = json.t_L;
    if(typeof json.t_E != "undefined") t_E = json.t_E;
    if(typeof json.J != "undefined") J = json.J;
    if(typeof json.IMax != "undefined") IMax = json.IMax;
    if(typeof json.Vin != "undefined") Vin = json.Vin;
    if(typeof json.Vmax != "undefined") Vmax = json.Vmax;
    if(typeof json.Vmin != "undefined") Vmin = json.Vmin;
    if(typeof json.Ptest != "undefined") Ptest = json.Ptest;
    if(typeof json.Etest != "undefined") Etest = json.Etest;
    if(typeof json.w0 != "undefined") w0 = json.w0;
    if(typeof json.I0 != "undefined") I0 = json.I0;
    if(typeof json.k2 != "undefined") k2 = json.k2;
    if(typeof json.b != "undefined") b = json.b;
    if(typeof json.height != "undefined") height = json.height;
    if(typeof json.width != "undefined") width = json.width;
    if(typeof json.heightGraph != "undefined") heightGraph = json.heightGraph;
    if(typeof json.widthGraph != "undefined") widthGraph = json.widthGraph;
    if(typeof json.heightGraph2 != "undefined") heightGraph2 = json.heightGraph2;
    if(typeof json.widthGraph2 != "undefined") widthGraph2 = json.widthGraph2;
    if(typeof json.width3D != "undefined") width3D = json.width3D;
    if(typeof json.height3D != "undefined") height3D = json.height3D;
    if(typeof json.DpropellerView != "undefined") DpropellerView = json.DpropellerView;
    if(typeof json.TpropellerView != "undefined") TpropellerView = json.TpropellerView;
    if(typeof json.DpropellerSelector != "undefined") DpropellerSelector = json.DpropellerSelector;
    if(typeof json.TpropellerSelector != "undefined") TpropellerSelector = json.TpropellerSelector;
    if(typeof json.zOffset != "undefined") zOffset = json.zOffset;
    if(typeof json.cameraAltitude != "undefined") cameraAltitude = json.cameraAltitude;
    if(typeof json.cameraAzimuth != "undefined") cameraAzimuth = json.cameraAzimuth;
    if(typeof json.cameraX != "undefined") cameraX = json.cameraX;
    if(typeof json.cameraY != "undefined") cameraY = json.cameraY;
    if(typeof json.cameraZ != "undefined") cameraZ = json.cameraZ;
    if(typeof json.cameraInitial != "undefined") cameraInitial = json.cameraInitial;
    if(typeof json.cameraAbove != "undefined") cameraAbove = json.cameraAbove;
    if(typeof json.cameraProjection != "undefined") cameraProjection = json.cameraProjection;
    if(typeof json.cameraTilt != "undefined") cameraTilt = json.cameraTilt;
    if(typeof json.cameraNear != "undefined") cameraNear = json.cameraNear;
    if(typeof json.cameraFar != "undefined") cameraFar = json.cameraFar;
    if(typeof json.cFX != "undefined") cFX = json.cFX;
    if(typeof json.cFY != "undefined") cFY = json.cFY;
    if(typeof json.cFZ != "undefined") cFZ = json.cFZ;
    if(typeof json.acrylicTransparency != "undefined") acrylicTransparency = json.acrylicTransparency;
    if(typeof json.vectorsView != "undefined") vectorsView = json.vectorsView;
    if(typeof json.taux != "undefined") taux = json.taux;
  };

  _model._readParametersPublic = function(json) {
    if(typeof json.theta0 != "undefined") theta0 = json.theta0;
    if(typeof json.z0 != "undefined") z0 = json.z0;
    if(typeof json.g != "undefined") g = json.g;
    if(typeof json.pi != "undefined") pi = json.pi;
    if(typeof json.rho != "undefined") rho = json.rho;
    if(typeof json.t != "undefined") t = json.t;
    if(typeof json.dt != "undefined") dt = json.dt;
    if(typeof json.Kp != "undefined") Kp = json.Kp;
    if(typeof json.Ki != "undefined") Ki = json.Ki;
    if(typeof json.Kd != "undefined") Kd = json.Kd;
    if(typeof json.errorP != "undefined") errorP = json.errorP;
    if(typeof json.errorI != "undefined") errorI = json.errorI;
    if(typeof json.P != "undefined") P = json.P;
    if(typeof json.I != "undefined") I = json.I;
    if(typeof json.D != "undefined") D = json.D;
    if(typeof json.theta != "undefined") theta = json.theta;
    if(typeof json.z != "undefined") z = json.z;
    if(typeof json.vT != "undefined") vT = json.vT;
    if(typeof json.zT != "undefined") zT = json.zT;
    if(typeof json.zMax != "undefined") zMax = json.zMax;
    if(typeof json.zMin != "undefined") zMin = json.zMin;
    if(typeof json.PM != "undefined") PM = json.PM;
    if(typeof json.vG != "undefined") vG = json.vG;
    if(typeof json.zSP != "undefined") zSP = json.zSP;
    if(typeof json.mMotor != "undefined") mMotor = json.mMotor;
    if(typeof json.mBar != "undefined") mBar = json.mBar;
    if(typeof json.mPropeller != "undefined") mPropeller = json.mPropeller;
    if(typeof json.m != "undefined") m = json.m;
    if(typeof json.N != "undefined") N = json.N;
    if(typeof json.diameterPropeller != "undefined") diameterPropeller = json.diameterPropeller;
    if(typeof json.r != "undefined") r = json.r;
    if(typeof json.Nblades != "undefined") Nblades = json.Nblades;
    if(typeof json.KV != "undefined") KV = json.KV;
    if(typeof json.i != "undefined") i = json.i;
    if(typeof json.R != "undefined") R = json.R;
    if(typeof json.U != "undefined") U = json.U;
    if(typeof json.w != "undefined") w = json.w;
    if(typeof json.L != "undefined") L = json.L;
    if(typeof json.kv != "undefined") kv = json.kv;
    if(typeof json.ke != "undefined") ke = json.ke;
    if(typeof json.th != "undefined") th = json.th;
    if(typeof json.t_L != "undefined") t_L = json.t_L;
    if(typeof json.t_E != "undefined") t_E = json.t_E;
    if(typeof json.J != "undefined") J = json.J;
    if(typeof json.IMax != "undefined") IMax = json.IMax;
    if(typeof json.Vin != "undefined") Vin = json.Vin;
    if(typeof json.Vmax != "undefined") Vmax = json.Vmax;
    if(typeof json.Vmin != "undefined") Vmin = json.Vmin;
    if(typeof json.Ptest != "undefined") Ptest = json.Ptest;
    if(typeof json.Etest != "undefined") Etest = json.Etest;
    if(typeof json.w0 != "undefined") w0 = json.w0;
    if(typeof json.I0 != "undefined") I0 = json.I0;
    if(typeof json.k2 != "undefined") k2 = json.k2;
    if(typeof json.b != "undefined") b = json.b;
    if(typeof json.height != "undefined") height = json.height;
    if(typeof json.width != "undefined") width = json.width;
    if(typeof json.heightGraph != "undefined") heightGraph = json.heightGraph;
    if(typeof json.widthGraph != "undefined") widthGraph = json.widthGraph;
    if(typeof json.heightGraph2 != "undefined") heightGraph2 = json.heightGraph2;
    if(typeof json.widthGraph2 != "undefined") widthGraph2 = json.widthGraph2;
    if(typeof json.width3D != "undefined") width3D = json.width3D;
    if(typeof json.height3D != "undefined") height3D = json.height3D;
    if(typeof json.DpropellerView != "undefined") DpropellerView = json.DpropellerView;
    if(typeof json.TpropellerView != "undefined") TpropellerView = json.TpropellerView;
    if(typeof json.DpropellerSelector != "undefined") DpropellerSelector = json.DpropellerSelector;
    if(typeof json.TpropellerSelector != "undefined") TpropellerSelector = json.TpropellerSelector;
    if(typeof json.zOffset != "undefined") zOffset = json.zOffset;
    if(typeof json.cameraAltitude != "undefined") cameraAltitude = json.cameraAltitude;
    if(typeof json.cameraAzimuth != "undefined") cameraAzimuth = json.cameraAzimuth;
    if(typeof json.cameraX != "undefined") cameraX = json.cameraX;
    if(typeof json.cameraY != "undefined") cameraY = json.cameraY;
    if(typeof json.cameraZ != "undefined") cameraZ = json.cameraZ;
    if(typeof json.cameraInitial != "undefined") cameraInitial = json.cameraInitial;
    if(typeof json.cameraAbove != "undefined") cameraAbove = json.cameraAbove;
    if(typeof json.cameraProjection != "undefined") cameraProjection = json.cameraProjection;
    if(typeof json.cameraTilt != "undefined") cameraTilt = json.cameraTilt;
    if(typeof json.cameraNear != "undefined") cameraNear = json.cameraNear;
    if(typeof json.cameraFar != "undefined") cameraFar = json.cameraFar;
    if(typeof json.cFX != "undefined") cFX = json.cFX;
    if(typeof json.cFY != "undefined") cFY = json.cFY;
    if(typeof json.cFZ != "undefined") cFZ = json.cFZ;
    if(typeof json.acrylicTransparency != "undefined") acrylicTransparency = json.acrylicTransparency;
    if(typeof json.vectorsView != "undefined") vectorsView = json.vectorsView;
    if(typeof json.taux != "undefined") taux = json.taux;
  };

  function _unserializePublic(json) { return _model.unserializePublic(json); }

  _model._userUnserializePublic = function(json) {
    _model._readParametersPublic(json);
   _resetSolvers();
   _model.update();
  };

  function _unserialize(json) { return _model.unserialize(json); }

  _model._userUnserialize = function(json) {
    _model._readParameters(json);
   _resetSolvers();
   _model.update();
  };

  _model.addToReset(function() {
    __pagesEnabled["startCode"] = true;
    __pagesEnabled["Página Evolución"] = true;
    __pagesEnabled["PID"] = true;
    __pagesEnabled["UI"] = true;
    __pagesEnabled["PID"] = true;
  });

  _model.addToReset(function() {
    theta0 = 0.7; // EjsS Model.Variables.Constants.theta0
    z0 = 25; // EjsS Model.Variables.Constants.z0
    g = 9.81; // EjsS Model.Variables.Constants.g
    pi = 3.14159265358979328462; // EjsS Model.Variables.Constants.pi
    rho = 1.290; // EjsS Model.Variables.Constants.rho
  });

  _model.addToReset(function() {
    t = 0; // EjsS Model.Variables.Dynamic.t
    dt = 0.005; // EjsS Model.Variables.Dynamic.dt
    Kp = 2; // EjsS Model.Variables.Dynamic.Kp
    Ki = 0.06; // EjsS Model.Variables.Dynamic.Ki
    Kd = 1.46; // EjsS Model.Variables.Dynamic.Kd
    errorP = 0; // EjsS Model.Variables.Dynamic.errorP
    errorI = 0; // EjsS Model.Variables.Dynamic.errorI
    P = 0; // EjsS Model.Variables.Dynamic.P
    I = 0; // EjsS Model.Variables.Dynamic.I
    D = 0; // EjsS Model.Variables.Dynamic.D
    theta = 0; // EjsS Model.Variables.Dynamic.theta
    z = 0; // EjsS Model.Variables.Dynamic.z
    vT = 0; // EjsS Model.Variables.Dynamic.vT
    zT = 0; // EjsS Model.Variables.Dynamic.zT
    zMax = 375-z0; // EjsS Model.Variables.Dynamic.zMax
    zMin = 0-z0; // EjsS Model.Variables.Dynamic.zMin
    PM = 0; // EjsS Model.Variables.Dynamic.PM
    vG = 3; // EjsS Model.Variables.Dynamic.vG
  });

  _model.addToReset(function() {
    zSP = 225; // EjsS Model.Variables.Parametric.zSP
    mMotor = 35; // EjsS Model.Variables.Parametric.mMotor
    mBar = 500; // EjsS Model.Variables.Parametric.mBar
    mPropeller = 10; // EjsS Model.Variables.Parametric.mPropeller
    m = (mMotor+mBar+mPropeller)/1000; // EjsS Model.Variables.Parametric.m
    N = 0.98; // EjsS Model.Variables.Parametric.N
    diameterPropeller = 0.25; // EjsS Model.Variables.Parametric.diameterPropeller
    r = diameterPropeller/2; // EjsS Model.Variables.Parametric.r
    Nblades = 2; // EjsS Model.Variables.Parametric.Nblades
    KV = 2400; // EjsS Model.Variables.Parametric.KV
    i = 35; // EjsS Model.Variables.Parametric.i
    R = 0.7; // EjsS Model.Variables.Parametric.R
    U = 7.33; // EjsS Model.Variables.Parametric.U
    w = 0; // EjsS Model.Variables.Parametric.w
    L = 0.05; // EjsS Model.Variables.Parametric.L
    kv = 0.0012; // EjsS Model.Variables.Parametric.kv
    ke = 7; // EjsS Model.Variables.Parametric.ke
    th = 0; // EjsS Model.Variables.Parametric.th
    t_L = 0.3; // EjsS Model.Variables.Parametric.t_L
    t_E = 0.7; // EjsS Model.Variables.Parametric.t_E
    J = 25; // EjsS Model.Variables.Parametric.J
    IMax = 38; // EjsS Model.Variables.Parametric.IMax
    Vin = 6; // EjsS Model.Variables.Parametric.Vin
    Vmax = 20; // EjsS Model.Variables.Parametric.Vmax
    Vmin = 0; // EjsS Model.Variables.Parametric.Vmin
    Ptest = 1; // EjsS Model.Variables.Parametric.Ptest
    Etest = 2; // EjsS Model.Variables.Parametric.Etest
    w0 = Vin/KV; // EjsS Model.Variables.Parametric.w0
    I0 = 24; // EjsS Model.Variables.Parametric.I0
    k2 = 50; // EjsS Model.Variables.Parametric.k2
    b = 2*pi*rho*r*r; // EjsS Model.Variables.Parametric.b
  });

  _model.addToReset(function() {
    height = 550; // EjsS Model.Variables.UI.height
    width = 450; // EjsS Model.Variables.UI.width
    heightGraph = 450; // EjsS Model.Variables.UI.heightGraph
    widthGraph = 750; // EjsS Model.Variables.UI.widthGraph
    heightGraph2 = heightGraph+50; // EjsS Model.Variables.UI.heightGraph2
    widthGraph2 = widthGraph; // EjsS Model.Variables.UI.widthGraph2
    width3D = widthGraph/2; // EjsS Model.Variables.UI.width3D
    height3D = heightGraph+50; // EjsS Model.Variables.UI.height3D
    DpropellerView = true; // EjsS Model.Variables.UI.DpropellerView
    TpropellerView = false; // EjsS Model.Variables.UI.TpropellerView
    DpropellerSelector = true; // EjsS Model.Variables.UI.DpropellerSelector
    TpropellerSelector = false; // EjsS Model.Variables.UI.TpropellerSelector
    zOffset = 175; // EjsS Model.Variables.UI.zOffset
    cameraAltitude = 0; // EjsS Model.Variables.UI.cameraAltitude
    cameraAzimuth = 90; // EjsS Model.Variables.UI.cameraAzimuth
    cameraX = 600; // EjsS Model.Variables.UI.cameraX
    cameraY = 0; // EjsS Model.Variables.UI.cameraY
    cameraZ = 50; // EjsS Model.Variables.UI.cameraZ
    cameraInitial = true; // EjsS Model.Variables.UI.cameraInitial
    cameraAbove = false; // EjsS Model.Variables.UI.cameraAbove
    cameraProjection = ""; // EjsS Model.Variables.UI.cameraProjection
    cameraTilt = 0; // EjsS Model.Variables.UI.cameraTilt
    cameraNear = 50; // EjsS Model.Variables.UI.cameraNear
    cameraFar = 2000; // EjsS Model.Variables.UI.cameraFar
    cFX = 10; // EjsS Model.Variables.UI.cFX
    cFY = 10; // EjsS Model.Variables.UI.cFY
    cFZ = 10; // EjsS Model.Variables.UI.cFZ
    acrylicTransparency = 240; // EjsS Model.Variables.UI.acrylicTransparency
    vectorsView = true; // EjsS Model.Variables.UI.vectorsView
    taux = 0; // EjsS Model.Variables.UI.taux
  });

  if (_inputParameters) {
    _inputParameters = _model.parseInputParameters(_inputParameters);
    if (_inputParameters) _model.addToReset(function() { _model._readParameters(_inputParameters); });
  }

  _model.addToReset(function() {
    _privateOdesList=[];
    _ODEi_evolution1 = _ODE_evolution1();
    _privateOdesList.push(_ODEi_evolution1);
  });

  _model.addToReset(function() {
    _model.setAutoplay(false);
    _model.setPauseOnPageExit(true);
    _model.setFPS(20);
    _model.setStepsPerDisplay(20);
  });

  function propellerSelector() {  // > CustomCode.graficas:1
    if (propeller==2) {  // > CustomCode.graficas:2
        _view.Dpropeller3D.linkProperty("Visibility", true );  // > CustomCode.graficas:3
        _view.Tpropeller3D.linkProperty("Visibility",  false );  // > CustomCode.graficas:4
      }  // > CustomCode.graficas:5
    if (propeller==3) {  // > CustomCode.graficas:6
        _view.Dpropeller3D.linkProperty("Visibility",  false );  // > CustomCode.graficas:7
        _view.Tpropeller3D.linkProperty("Visibility", true );  // > CustomCode.graficas:8
      }  // > CustomCode.graficas:9
  }  // > CustomCode.graficas:10

  function toZero(A){  // > CustomCode.variables:1
    A=0.0;  // > CustomCode.variables:2
  } // doesnt seem to work when called  // > CustomCode.variables:3
  function UZero() {  // > CustomCode.variables:4
    U=0.0;  // > CustomCode.variables:5
  }  // > CustomCode.variables:6
  function PZero () {  // > CustomCode.variables:7
    Kp = 0.0;   // > CustomCode.variables:8
  }  // > CustomCode.variables:9
  function IZero (){  // > CustomCode.variables:10
    Ki = 0.0;  // > CustomCode.variables:11
  }  // > CustomCode.variables:12
  function DZero (){  // > CustomCode.variables:13
    Kd = 0.0;  // > CustomCode.variables:14
  }  // > CustomCode.variables:15

  _model.addToInitialization(function() {
    if (!__pagesEnabled["startCode"]) return;
    propellerSelector=2;  // > Initialization.startCode:1
    t=0;  // > Initialization.startCode:2
    // Show which propeller is shown at the initialitaion  // > Initialization.startCode:3
    DpropellerSelector = true;  // > Initialization.startCode:4
    DpropellerView = true;  // > Initialization.startCode:5
    TpropellerSelector = false;  // > Initialization.startCode:6
    TpropellerView = false;  // > Initialization.startCode:7
  });

  _model.addToInitialization(function() {
    _initializeSolvers();
  });

  _model.addToEvolution(function() {
    if (!__pagesEnabled["Página Evolución"]) return;
    _ODEi_evolution1.step();
  });

  _model.addToEvolution(function() {
    if (!__pagesEnabled["PID"]) return;
    // PID CONTROL FUNCTION  // > Evolution.PID:1
    // Can be used with any other variable apart from Voltage.   // > Evolution.PID:2
    // Input arguments are 'x' (current value of the variable) and   // > Evolution.PID:3
    // 'xSP' (set point of the desired variable)  // > Evolution.PID:4
    errorP = 0 ; // Already defined  // > Evolution.PID:5
    errorI = 0 ; // Already defined  // > Evolution.PID:6
    function PIDControl(x,xSP){  // > Evolution.PID:7
      x=x/1; // xGain (Volts/Unit)  // > Evolution.PID:8
      x=x+0; // xBias (Unit)  // > Evolution.PID:9
        // > Evolution.PID:10
      P=(xSP-x); // Proportional action (Units = px)  // > Evolution.PID:11
      var Iprev = I;  // > Evolution.PID:12
      I=(P*dt)+I; // Integral action  // > Evolution.PID:13
      D=(P-errorP)/dt; // Derivative action  // > Evolution.PID:14
      var u = (Kp*P)+(Ki*I)+(Kd*D);  // > Evolution.PID:15
      var v = (u>Vmax) ? Vmax : (u<=Vmin ? Vmin : u);// Limit output maximum  // > Evolution.PID:16
      if((errorP * (v-u)) < 0) {  // > Evolution.PID:17
        I = Iprev;  // > Evolution.PID:18
      } //Windup   // > Evolution.PID:19
      errorP=P; // Error of the controller. Set 0 by default, reset every loop  // > Evolution.PID:20
        // > Evolution.PID:21
      return v; // PID Output as the sum of every   // > Evolution.PID:22
    }  // > Evolution.PID:23
    // Declare voltage output from the controller. Units converted to Vmax. Gain, outside the function  // > Evolution.PID:24
    U = PIDControl(z,zSP);  // > Evolution.PID:25
  });

  _model.addToFixedRelations(function() { _isPaused = _model.isPaused(); _isPlaying = _model.isPlaying(); });

  _model.addToFixedRelations(function() {
    if (!__pagesEnabled["UI"]) return;
    // Visual effect of the propeller rotation (NEEDS IMPROVEMENT)  // > FixedRelations.UI:1
    theta=Math.abs(20/8.32*Vin*t);   // > FixedRelations.UI:2
    b = (2*pi*rho*r^2)*(1/3);  // > FixedRelations.UI:3
    //Final value for Z, and prior to the loop control  // > FixedRelations.UI:4
    z=z0+zT;  // > FixedRelations.UI:5
    // Limit position of the motor. Boundaries of the system  // > FixedRelations.UI:6
    if (z >= zMax){  // > FixedRelations.UI:7
      z=zMax;  // > FixedRelations.UI:8
    } else if (z <= zMin){  // > FixedRelations.UI:9
      z=zMin;  // > FixedRelations.UI:10
    }   // > FixedRelations.UI:11
    // Limit of the current  // > FixedRelations.UI:12
    //Dynamic t axis   // > FixedRelations.UI:13
    if (t<=25) {  // > FixedRelations.UI:14
      taux=0;}  // > FixedRelations.UI:15
    else {  // > FixedRelations.UI:16
      taux=t-25}  // > FixedRelations.UI:17
    // Tests  // > FixedRelations.UI:18
    Ptest=-m*g;  // > FixedRelations.UI:19
    Etest=+m*g*Vin/6;  // > FixedRelations.UI:20
  });

  _model.addToFixedRelations(function() {
    if (!__pagesEnabled["PID"]) return;
  });

  _model.addToFixedRelations(function() { _isPaused = _model.isPaused(); _isPlaying = _model.isPlaying(); });

  function _initializeSolvers() {
    for (var i=0,n=_privateOdesList.length; i<n; i++) _privateOdesList[i].initializeSolver();
  }

  function _automaticResetSolvers() {
    for (var i=0,n=_privateOdesList.length; i<n; i++) _privateOdesList[i].automaticResetSolver();
  }

  _model.resetSolvers = function() {
    for (var i=0,n=_privateOdesList.length; i<n; i++) _privateOdesList[i].resetSolver();
  };

  _getODE = function (_odeName) {
    if (_odeName=="Página Evolución") return _ODEi_evolution1;
    return null;
  }

  function _getEventSolver(_odeName) {
    var ode = _getODE(_odeName);
    if (ode===null) return null;
    return ode.getEventSolver();
  }

  function _setSolverClass(_odeName, _engine) {
    var ode = _getODE(_odeName);
    if (ode===null) return;
    if (!_engine.setODE) {
      var classname = _engine.toLowerCase();
      if      (classname.indexOf("boga")>=0)   _engine = EJSS_ODE_SOLVERS.bogackiShampine23;
      else if (classname.indexOf("cash")>=0)   _engine = EJSS_ODE_SOLVERS.cashKarp45;
      else if (classname.indexOf("dopri5")>=0) _engine = EJSS_ODE_SOLVERS.dopri5;
      else if (classname.indexOf("dopri8")>=0) _engine = EJSS_ODE_SOLVERS.dopri853;
      else if (classname.indexOf("richa")>=0)  _engine = EJSS_ODE_SOLVERS.eulerRichardson;
      else if (classname.indexOf("euler")>=0)  _engine = EJSS_ODE_SOLVERS.euler;
      else if (classname.indexOf("fehlberg87")>=0) _engine = EJSS_ODE_SOLVERS.fehlberg87;
      else if (classname.indexOf("fehlberg8")>=0)  _engine = EJSS_ODE_SOLVERS.fehlberg8;
      else if (classname.indexOf("radau")>=0)   _engine = EJSS_ODE_SOLVERS.radau5;
      else if (classname.indexOf("runge")>=0)  _engine = EJSS_ODE_SOLVERS.rungeKutta4;
      else if (classname.indexOf("rk4")>=0)    _engine = EJSS_ODE_SOLVERS.rungeKutta4;
      else if (classname.indexOf("verlet")>=0) _engine = EJSS_ODE_SOLVERS.velocityVerlet;
    }
    if (_engine) ode.setSolverClass(_engine);
  }

  function _ODE_evolution1() {
    var __odeSelf = {};
    var __eventSolver;
    var __solverClass = EJSS_ODE_SOLVERS.rungeKutta4;
    var __state=[];
    var __ignoreErrors=false;
    var __mustInitialize=true;
    var __isEnabled=true;
    var __mustUserReinitialize=false;
    var __mustReinitialize=true;


    __odeSelf._getOdeVars = function (){ return["vT","zT","i","th","w","t"]};

    __odeSelf.setSolverClass = function(__aSolverClass) {
      __solverClass = __aSolverClass;
      __instantiateSolver();
    };

    function __instantiateSolver() {
      __state=[];
      __pushState();
      __eventSolver = EJSS_ODE_SOLVERS.interpolatorEventSolver(__solverClass(),__odeSelf);
      __mustInitialize = true;
    }

    __odeSelf.setEnabled = function(_enabled) { __isEnabled = _enabled; };

    __odeSelf.getIndependentVariableValue = function() { return __eventSolver.getIndependentVariableValue(); };

    __odeSelf.getInternalStepSize = function() { return __eventSolver.getInternalStepSize(); };

    __odeSelf.isAccelerationIndependentOfVelocity = function() { return false; };

    __odeSelf.initializeSolver = function() {
      if (__arraysChanged()) { __instantiateSolver(); __odeSelf.initializeSolver(); return; }
      __pushState();
      __eventSolver.initialize(dt);
      __eventSolver.setBestInterpolation(false);
      __eventSolver.setMaximumInternalSteps(10000);
      __eventSolver.removeAllEvents();
      for(k in userEvents1){__eventSolver.addEvent(userEvents1[k]);}
      __eventSolver.setEstimateFirstStep(false);
      __eventSolver.setEnableExceptions(false);
      __eventSolver.setTolerances(0.00001,0.00001);
      __mustReinitialize = true;
      __mustInitialize = false;
    };

    function __pushState() {
      // Copy our variables to __state[] 
        var __j=0;
        var __n=0;
        var __cIn=0;
        if (__state[__cIn]!=vT) __mustReinitialize = true;
        __state[__cIn++] = vT;
        if (__state[__cIn]!=zT) __mustReinitialize = true;
        __state[__cIn++] = zT;
        if (__state[__cIn]!=i) __mustReinitialize = true;
        __state[__cIn++] = i;
        if (__state[__cIn]!=th) __mustReinitialize = true;
        __state[__cIn++] = th;
        if (__state[__cIn]!=w) __mustReinitialize = true;
        __state[__cIn++] = w;
        if (__state[__cIn]!=t) __mustReinitialize = true;
        __state[__cIn++] = t;
    }

    function __arraysChanged () {
      return false;
    }

    __odeSelf.getEventSolver = function() {
      return __eventSolver;
    };

    __odeSelf.resetSolver = function() {
      __mustUserReinitialize = true;
    };

    __odeSelf.automaticResetSolver = function() {
      __mustReinitialize = true;
    };

    function __errorAction () {
      if (__ignoreErrors) return;
      console.log (__eventSolver.getErrorMessage());
      _pause();
      // Make sure the solver is reinitialized;
      __mustReinitialize = true;
    }

    __odeSelf.step = function() { return __privateStep(false); };

    __odeSelf.solverStep = function() { return __privateStep(true); };

    function __privateStep(__takeMaximumStep) {
      if (!__isEnabled) return 0;
      if (dt===0) return 0;
      if (__mustInitialize) __odeSelf.initializeSolver();
      if (__arraysChanged()) { __instantiateSolver(); __odeSelf.initializeSolver(); }
      __eventSolver.setStepSize(dt);
      __eventSolver.setInternalStepSize(dt);
      __eventSolver.setMaximumInternalSteps(10000);
      __eventSolver.setTolerances(0.00001,0.00001);
      __pushState();
      if (__mustUserReinitialize) { 
        __eventSolver.userReinitialize();
        __mustUserReinitialize = false;
        __mustReinitialize = false;
        if (__eventSolver.getErrorCode()!=EJSS_ODE_SOLVERS.ERROR.NO_ERROR) __errorAction();
      }
      else if (__mustReinitialize) { 
        __eventSolver.reinitialize();
        __mustReinitialize = false;
        if (__eventSolver.getErrorCode()!=EJSS_ODE_SOLVERS.ERROR.NO_ERROR) __errorAction();
      }
      var __stepTaken = __takeMaximumStep ? __eventSolver.maxStep() : __eventSolver.step();
      // Extract our variables from __state
        var __i=0;
        var __cOut=0;
        vT = __state[__cOut++];
        zT = __state[__cOut++];
        i = __state[__cOut++];
        th = __state[__cOut++];
        w = __state[__cOut++];
        t = __state[__cOut++];
      // Check for error
      if (__eventSolver.getErrorCode()!=EJSS_ODE_SOLVERS.ERROR.NO_ERROR) __errorAction();
      return __stepTaken;
    }

    __odeSelf.getState = function() { return __state; };

    __odeSelf.getRate = function(_aState,_aRate) {
      _aRate[_aRate.length-1] = 0.0; // In case the prelim code returns
      var __index=-1; // so that it can be used in preliminary code
      // Extract our variables from _aState
        var __i=0;
        var __cOut=0;
        var vT = _aState[__cOut++];
        var zT = _aState[__cOut++];
        var i = _aState[__cOut++];
        var th = _aState[__cOut++];
        var w = _aState[__cOut++];
        var t = _aState[__cOut++];
      // Preliminary code: Código a ejecutar antes de evaluar las derivadas de las ecuaciones diferenciales
        // Pares de fuerzas  // > Preliminary code for ODE.Página Evolución:1
        t_e = ke*w;  // > Preliminary code for ODE.Página Evolución:2
        t_L = U*i/(2*pi/60*w);   // > Preliminary code for ODE.Página Evolución:3
        //Potencia  // > Preliminary code for ODE.Página Evolución:4
        PM=N*i*U;  // > Preliminary code for ODE.Página Evolución:5
      // Compute the rate
        var __cRate=0;
        _aRate[__cRate++] = b*Math.pow(w,2)-g; // Rate for ODE: Página Evolución:vT
        _aRate[__cRate++] = vT; // Rate for ODE: Página Evolución:zT
        _aRate[__cRate++] = (-i*R-kv*w+U)/L; // Rate for ODE: Página Evolución:i
        _aRate[__cRate++] = w; // Rate for ODE: Página Evolución:th
        _aRate[__cRate++] = (ke*i-k2*w)/J; // Rate for ODE: Página Evolución:w
        _aRate[__cRate++] = 1; // independent variable
        return _aRate;
    }; //end of getRate

    __odeSelf._addEvent = function(userCondition,userAction,eventType,eventMethod,maxIter,eventTolerance,endAtEvent){
    var User_Event = function (userCondition,userAction,eventType,eventMethod,maxIter,eventTolerance,endAtEvent) {
      var _eventSelf = {};

      _eventSelf.getTypeOfEvent = function() { return eventType; };

      _eventSelf.getRootFindingMethod = function() { return eventMethod; };

      _eventSelf.getMaxIterations = function() { return maxIter; };

      _eventSelf.getTolerance = function() { return eventTolerance; };

      _eventSelf.evaluate = function(_aState) { 
      // Extract our variables from _aState
        var __i=0;
        var __cOut=0;
        var vT = _aState[__cOut++];
        var zT = _aState[__cOut++];
        var i = _aState[__cOut++];
        var th = _aState[__cOut++];
        var w = _aState[__cOut++];
        var t = _aState[__cOut++];
      return eval(userCondition);
      };

      _eventSelf.action = function() { 
      // Extract our variables from __state
        var __i=0;
        var __cOut=0;
        vT = __state[__cOut++];
        zT = __state[__cOut++];
        i = __state[__cOut++];
        th = __state[__cOut++];
        w = __state[__cOut++];
        t = __state[__cOut++];
        var _returnValue = __userDefinedAction();
      // Copy our variables to __state[] 
        var __j=0;
        var __n=0;
        var __cIn=0;
        __state[__cIn++] = vT;
        __state[__cIn++] = zT;
        __state[__cIn++] = i;
        __state[__cIn++] = th;
        __state[__cIn++] = w;
        __state[__cIn++] = t;
        return _returnValue;
      };

      function __userDefinedAction() {
        if (undefined != functions) eval(functions.toString());
        eval(userAction);
        return endAtEvent;
      }

      return _eventSelf;
    }; // End of event

   userEvents1.push(User_Event(userCondition,userAction,eventType,eventMethod,maxIter,eventTolerance,endAtEvent));
   }

    __instantiateSolver();

    return __odeSelf;
  }

  function _historic_vT(__time) {
    var __index = 0;
    return _ODEi_evolution1.getEventSolver().getStateHistory().interpolate(__time,__index);
  }

  function _historic_zT(__time) {
    var __index = 0 + 1;
    return _ODEi_evolution1.getEventSolver().getStateHistory().interpolate(__time,__index);
  }

  function _historic_i(__time) {
    var __index = 0 + 1 + 1;
    return _ODEi_evolution1.getEventSolver().getStateHistory().interpolate(__time,__index);
  }

  function _historic_th(__time) {
    var __index = 0 + 1 + 1 + 1;
    return _ODEi_evolution1.getEventSolver().getStateHistory().interpolate(__time,__index);
  }

  function _historic_w(__time) {
    var __index = 0 + 1 + 1 + 1 + 1;
    return _ODEi_evolution1.getEventSolver().getStateHistory().interpolate(__time,__index);
  }

    _model._fontResized = function(iBase,iSize,iDelta) {
      _view._fontResized(iBase,iSize,iDelta);
  }; // end of _fontResized

  function _getViews() {
    var _viewsInfo = [];
    var _counter = 0;
    _viewsInfo[_counter++] = { name : "Vista", width : 800, height : 600 };
    return _viewsInfo;
  } // end of _getViews

  function _selectView(_viewNumber) {
    _view = null;
    _view = new tfg10_View(_topFrame,_viewNumber,_libraryPath,_codebasePath);
    var _view_super_reset = _view._reset;
    _view._reset = function() {
      _view_super_reset();
      switch(_viewNumber) {
        case -10 : break; // make Lint happy
        default :
        case 0:
          _view.main_left.linkProperty("Height",  function() { return height; }, function(_v) { height = _v; } ); // Vista linking property 'Height' for element 'main_left'
          _view.panelDibujo3D.linkProperty("Height",  function() { return height3D; }, function(_v) { height3D = _v; } ); // Vista linking property 'Height' for element 'panelDibujo3D'
          _view.panelDibujo3D.linkProperty("Width",  function() { return width3D; }, function(_v) { width3D = _v; } ); // Vista linking property 'Width' for element 'panelDibujo3D'
          _view.panelDibujo3D.linkProperty("CameraTilt",  function() { return cameraTilt; }, function(_v) { cameraTilt = _v; } ); // Vista linking property 'CameraTilt' for element 'panelDibujo3D'
          _view.panelDibujo3D.linkProperty("CameraZ",  function() { return cameraZ; }, function(_v) { cameraZ = _v; } ); // Vista linking property 'CameraZ' for element 'panelDibujo3D'
          _view.panelDibujo3D.linkProperty("CSS",  function() { return { "margin-top":"35px",   "border-radius": "10px", }; } ); // Vista linking property 'CSS' for element 'panelDibujo3D'
          _view.panelDibujo3D.linkProperty("CameraY",  function() { return cameraY; }, function(_v) { cameraY = _v; } ); // Vista linking property 'CameraY' for element 'panelDibujo3D'
          _view.panelDibujo3D.linkProperty("CameraX",  function() { return cameraX; }, function(_v) { cameraX = _v; } ); // Vista linking property 'CameraX' for element 'panelDibujo3D'
          _view.panelDibujo3D.linkProperty("CameraAltitude",  function() { return cameraAltitude; }, function(_v) { cameraAltitude = _v; } ); // Vista linking property 'CameraAltitude' for element 'panelDibujo3D'
          _view.panelDibujo3D.linkProperty("CameraNear",  function() { return cameraNear; }, function(_v) { cameraNear = _v; } ); // Vista linking property 'CameraNear' for element 'panelDibujo3D'
          _view.panelDibujo3D.setAction("OnDrag", function(_data,_info) {
  cameraInitial=false;

}); // Vista setting action 'OnDrag' for element 'panelDibujo3D'
          _view.panelDibujo3D.linkProperty("CameraAzimuth",  function() { return cameraAzimuth; }, function(_v) { cameraAzimuth = _v; } ); // Vista linking property 'CameraAzimuth' for element 'panelDibujo3D'
          _view.panelDibujo3D.linkProperty("CameraFar",  function() { return cameraFar; }, function(_v) { cameraFar = _v; } ); // Vista linking property 'CameraFar' for element 'panelDibujo3D'
          _view.motor3D.linkProperty("Z",  function() { return z+z0-zOffset; } ); // Vista linking property 'Z' for element 'motor3D'
          _view.platform3D.linkProperty("Z",  function() { return -zOffset; } ); // Vista linking property 'Z' for element 'platform3D'
          _view.case3D.linkProperty("Transparency",  function() { return acrylicTransparency; }, function(_v) { acrylicTransparency = _v; } ); // Vista linking property 'Transparency' for element 'case3D'
          _view.case3D.linkProperty("Z",  function() { return -zOffset; } ); // Vista linking property 'Z' for element 'case3D'
          _view.caseTop3D.linkProperty("Transparency",  function() { return acrylicTransparency; }, function(_v) { acrylicTransparency = _v; } ); // Vista linking property 'Transparency' for element 'caseTop3D'
          _view.caseTop3D.linkProperty("Z",  function() { return -zOffset; } ); // Vista linking property 'Z' for element 'caseTop3D'
          _view.Dpropeller3D.linkProperty("Z",  function() { return z+z0-zOffset; } ); // Vista linking property 'Z' for element 'Dpropeller3D'
          _view.Dpropeller3D.linkProperty("Visibility",  function() { return DpropellerView; }, function(_v) { DpropellerView = _v; } ); // Vista linking property 'Visibility' for element 'Dpropeller3D'
          _view.DpropellerRotation.linkProperty("Angle",  function() { return theta; }, function(_v) { theta = _v; } ); // Vista linking property 'Angle' for element 'DpropellerRotation'
          _view.Tpropeller3D.linkProperty("Z",  function() { return z+z0-zOffset; } ); // Vista linking property 'Z' for element 'Tpropeller3D'
          _view.Tpropeller3D.linkProperty("Visibility",  function() { return TpropellerView; }, function(_v) { TpropellerView = _v; } ); // Vista linking property 'Visibility' for element 'Tpropeller3D'
          _view.TpropellerRotation.linkProperty("Angle",  function() { return theta; }, function(_v) { theta = _v; } ); // Vista linking property 'Angle' for element 'TpropellerRotation'
          _view.verticalBar3D.linkProperty("Z",  function() { return z+z0-zOffset; } ); // Vista linking property 'Z' for element 'verticalBar3D'
          _view.leftRod3D.linkProperty("Z",  function() { return -zOffset; } ); // Vista linking property 'Z' for element 'leftRod3D'
          _view.rightRod3D.linkProperty("Z",  function() { return -zOffset; } ); // Vista linking property 'Z' for element 'rightRod3D'
          _view.weightVector.linkProperty("Z",  function() { return z+z0-zOffset+30; } ); // Vista linking property 'Z' for element 'weightVector'
          _view.weightVector.linkProperty("Visibility",  function() { return vectorsView; }, function(_v) { vectorsView = _v; } ); // Vista linking property 'Visibility' for element 'weightVector'
          _view.weightLabel.linkProperty("Z",  function() { return z+z0-zOffset+30-65; } ); // Vista linking property 'Z' for element 'weightLabel'
          _view.weightLabel.linkProperty("Visibility",  function() { return vectorsView; }, function(_v) { vectorsView = _v; } ); // Vista linking property 'Visibility' for element 'weightLabel'
          _view.thrustVector.linkProperty("SizeZ",  function() { return 8.33333*Vin; } ); // Vista linking property 'SizeZ' for element 'thrustVector'
          _view.thrustVector.linkProperty("Z",  function() { return z+z0-zOffset+50; } ); // Vista linking property 'Z' for element 'thrustVector'
          _view.thrustVector.linkProperty("Visibility",  function() { return vectorsView; }, function(_v) { vectorsView = _v; } ); // Vista linking property 'Visibility' for element 'thrustVector'
          _view.thrustLabel.linkProperty("Z",  function() { return z+z0-zOffset+50+65+Vin*2.5; } ); // Vista linking property 'Z' for element 'thrustLabel'
          _view.thrustLabel.linkProperty("Visibility",  function() { return vectorsView; }, function(_v) { vectorsView = _v; } ); // Vista linking property 'Visibility' for element 'thrustLabel'
          _view.zDesired.linkProperty("Z",  function() { return zSP-zOffset+50; } ); // Vista linking property 'Z' for element 'zDesired'
          _view.zDesired.linkProperty("Visibility",  function() { return vectorsView; }, function(_v) { vectorsView = _v; } ); // Vista linking property 'Visibility' for element 'zDesired'
          _view.tabsPanel.linkProperty("Titles",  function() { return ["Altura","Voltaje","Ambas","Ajustes","tests"]; } ); // Vista linking property 'Titles' for element 'tabsPanel'
          _view.heightTab.linkProperty("Height",  function() { return heightGraph2; }, function(_v) { heightGraph2 = _v; } ); // Vista linking property 'Height' for element 'heightTab'
          _view.heightGraph.linkProperty("Height",  function() { return heightGraph2; }, function(_v) { heightGraph2 = _v; } ); // Vista linking property 'Height' for element 'heightGraph'
          _view.heightGraph.linkProperty("Width",  function() { return widthGraph2; }, function(_v) { widthGraph2 = _v; } ); // Vista linking property 'Width' for element 'heightGraph'
          _view.heightGraph.linkProperty("MinimumX",  function() { return taux; }, function(_v) { taux = _v; } ); // Vista linking property 'MinimumX' for element 'heightGraph'
          _view.heightGraph.linkProperty("MaximumX",  function() { return taux+25; } ); // Vista linking property 'MaximumX' for element 'heightGraph'
          _view.heightLine.linkProperty("InputX",  function() { return t; }, function(_v) { t = _v; } ); // Vista linking property 'InputX' for element 'heightLine'
          _view.heightLine.linkProperty("InputY",  function() { return z-z0+50; } ); // Vista linking property 'InputY' for element 'heightLine'
          _view.zSetPointLine.linkProperty("Y",  function() { return zSP+25; } ); // Vista linking property 'Y' for element 'zSetPointLine'
          _view.rastro.linkProperty("InputX",  function() { return t; }, function(_v) { t = _v; } ); // Vista linking property 'InputX' for element 'rastro'
          _view.rastro.linkProperty("InputY",  function() { return i; }, function(_v) { i = _v; } ); // Vista linking property 'InputY' for element 'rastro'
          _view.rastro2.linkProperty("InputX",  function() { return t; }, function(_v) { t = _v; } ); // Vista linking property 'InputX' for element 'rastro2'
          _view.rastro2.linkProperty("InputY",  function() { return w; }, function(_v) { w = _v; } ); // Vista linking property 'InputY' for element 'rastro2'
          _view.voltajeTab.linkProperty("Height",  function() { return heightGraph2; }, function(_v) { heightGraph2 = _v; } ); // Vista linking property 'Height' for element 'voltajeTab'
          _view.voltajeGraph.linkProperty("Height",  function() { return heightGraph2; }, function(_v) { heightGraph2 = _v; } ); // Vista linking property 'Height' for element 'voltajeGraph'
          _view.voltajeGraph.linkProperty("Width",  function() { return widthGraph2; }, function(_v) { widthGraph2 = _v; } ); // Vista linking property 'Width' for element 'voltajeGraph'
          _view.voltajeGraph.linkProperty("MaximumY",  function() { return Vmax; }, function(_v) { Vmax = _v; } ); // Vista linking property 'MaximumY' for element 'voltajeGraph'
          _view.voltajeGraph.linkProperty("MaximumX",  function() { return taux+25; } ); // Vista linking property 'MaximumX' for element 'voltajeGraph'
          _view.voltajeGraph.linkProperty("MinimumX",  function() { return taux; }, function(_v) { taux = _v; } ); // Vista linking property 'MinimumX' for element 'voltajeGraph'
          _view.voltajeGraph.linkProperty("MinimumY",  function() { return Vmin; }, function(_v) { Vmin = _v; } ); // Vista linking property 'MinimumY' for element 'voltajeGraph'
          _view.voltajeLine.linkProperty("InputX",  function() { return t; }, function(_v) { t = _v; } ); // Vista linking property 'InputX' for element 'voltajeLine'
          _view.voltajeLine.linkProperty("InputY",  function() { return U; }, function(_v) { U = _v; } ); // Vista linking property 'InputY' for element 'voltajeLine'
          _view.bothTab.linkProperty("Height",  function() { return heightGraph2; }, function(_v) { heightGraph2 = _v; } ); // Vista linking property 'Height' for element 'bothTab'
          _view.bothGraph.linkProperty("Height",  function() { return heightGraph2; }, function(_v) { heightGraph2 = _v; } ); // Vista linking property 'Height' for element 'bothGraph'
          _view.bothGraph.linkProperty("Width",  function() { return widthGraph2; }, function(_v) { widthGraph2 = _v; } ); // Vista linking property 'Width' for element 'bothGraph'
          _view.heightLine2.linkProperty("InputX",  function() { return t; }, function(_v) { t = _v; } ); // Vista linking property 'InputX' for element 'heightLine2'
          _view.heightLine2.linkProperty("InputY",  function() { return z-z0; } ); // Vista linking property 'InputY' for element 'heightLine2'
          _view.voltajeLine2.linkProperty("InputX",  function() { return t; }, function(_v) { t = _v; } ); // Vista linking property 'InputX' for element 'voltajeLine2'
          _view.voltajeLine2.linkProperty("InputY",  function() { return Vin; }, function(_v) { Vin = _v; } ); // Vista linking property 'InputY' for element 'voltajeLine2'
          _view.currentLine2.linkProperty("InputX",  function() { return t; }, function(_v) { t = _v; } ); // Vista linking property 'InputX' for element 'currentLine2'
          _view.currentLine2.linkProperty("InputY",  function() { return i; }, function(_v) { i = _v; } ); // Vista linking property 'InputY' for element 'currentLine2'
          _view.settingsTab.linkProperty("Height",  function() { return heightGraph2; }, function(_v) { heightGraph2 = _v; } ); // Vista linking property 'Height' for element 'settingsTab'
          _view.massMotorValue.linkProperty("Value",  function() { return mMotor; }, function(_v) { mMotor = _v; } ); // Vista linking property 'Value' for element 'massMotorValue'
          _view.currentMotorValue2.linkProperty("Value",  function() { return i; }, function(_v) { i = _v; } ); // Vista linking property 'Value' for element 'currentMotorValue2'
          _view.currentMaxMotorValue.linkProperty("Value",  function() { return IMax; }, function(_v) { IMax = _v; } ); // Vista linking property 'Value' for element 'currentMaxMotorValue'
          _view.inductanciaValue.linkProperty("Value",  function() { return L; }, function(_v) { L = _v; } ); // Vista linking property 'Value' for element 'inductanciaValue'
          _view.resistanceValue.linkProperty("Value",  function() { return R; }, function(_v) { R = _v; } ); // Vista linking property 'Value' for element 'resistanceValue'
          _view.kvValue.linkProperty("Value",  function() { return kv; }, function(_v) { kv = _v; } ); // Vista linking property 'Value' for element 'kvValue'
          _view.keValue.linkProperty("Value",  function() { return ke; }, function(_v) { ke = _v; } ); // Vista linking property 'Value' for element 'keValue'
          _view.efficiencyMotorValue.linkProperty("Value",  function() { return N; }, function(_v) { N = _v; } ); // Vista linking property 'Value' for element 'efficiencyMotorValue'
          _view.efficiencyMotorValue.setAction("OnChange", function(_data,_info) {
  if (N>=1){
    N=1;
    }

}); // Vista setting action 'OnChange' for element 'efficiencyMotorValue'
          _view.DpropellerSelector.linkProperty("Checked",  function() { return DpropellerSelector; }, function(_v) { DpropellerSelector = _v; } ); // Vista linking property 'Checked' for element 'DpropellerSelector'
          _view.DpropellerSelector.setAction("OnCheckOff", function(_data,_info) {
  TpropellerSelector = true;

}); // Vista setting action 'OnCheckOff' for element 'DpropellerSelector'
          _view.DpropellerSelector.setAction("OnCheckOn", function(_data,_info) {
  DpropellerView= true;
  TpropellerView= false;
  TpropellerSelector = false;
  Nblades=2;

}); // Vista setting action 'OnCheckOn' for element 'DpropellerSelector'
          _view.TpropellerSelector.linkProperty("Checked",  function() { return TpropellerSelector; }, function(_v) { TpropellerSelector = _v; } ); // Vista linking property 'Checked' for element 'TpropellerSelector'
          _view.TpropellerSelector.setAction("OnCheckOff", function(_data,_info) {
  DpropellerSelector= true;

}); // Vista setting action 'OnCheckOff' for element 'TpropellerSelector'
          _view.TpropellerSelector.setAction("OnCheckOn", function(_data,_info) {
  TpropellerView= true;
  DpropellerView= false;
  DpropellerSelector= false;
  Nblades=3;

}); // Vista setting action 'OnCheckOn' for element 'TpropellerSelector'
          _view.NpropellerValue.linkProperty("Value",  function() { return Nblades; }, function(_v) { Nblades = _v; } ); // Vista linking property 'Value' for element 'NpropellerValue'
          _view.massPropellerValue.linkProperty("Value",  function() { return mPropeller; }, function(_v) { mPropeller = _v; } ); // Vista linking property 'Value' for element 'massPropellerValue'
          _view.diameterPropellerValue.linkProperty("Value",  function() { return diameterPropeller; }, function(_v) { diameterPropeller = _v; } ); // Vista linking property 'Value' for element 'diameterPropellerValue'
          _view.viewDimensions.setAction("OnCheckOff", function(_data,_info) {
  cameraProjection="";

}); // Vista setting action 'OnCheckOff' for element 'viewDimensions'
          _view.viewDimensions.setAction("OnCheckOn", function(_data,_info) {
  cameraProjection="PERSPECTIVE_OFF";

}); // Vista setting action 'OnCheckOn' for element 'viewDimensions'
          _view.transparencyCase.setAction("OnCheckOff", function(_data,_info) {
  acrylicTransparency=255;

}); // Vista setting action 'OnCheckOff' for element 'transparencyCase'
          _view.transparencyCase.setAction("OnCheckOn", function(_data,_info) {
  acrylicTransparency=240;

}); // Vista setting action 'OnCheckOn' for element 'transparencyCase'
          _view.cameraAltitude.linkProperty("Value",  function() { return cameraAltitude; }, function(_v) { cameraAltitude = _v; } ); // Vista linking property 'Value' for element 'cameraAltitude'
          _view.cameraAzimuth.linkProperty("Value",  function() { return cameraAzimuth; }, function(_v) { cameraAzimuth = _v; } ); // Vista linking property 'Value' for element 'cameraAzimuth'
          _view.zOffset.linkProperty("Value",  function() { return zOffset; }, function(_v) { zOffset = _v; } ); // Vista linking property 'Value' for element 'zOffset'
          _view.cameraX.linkProperty("Value",  function() { return cameraX; }, function(_v) { cameraX = _v; } ); // Vista linking property 'Value' for element 'cameraX'
          _view.cameraY.linkProperty("Value",  function() { return cameraY; }, function(_v) { cameraY = _v; } ); // Vista linking property 'Value' for element 'cameraY'
          _view.cameraZ.linkProperty("Value",  function() { return cameraZ; }, function(_v) { cameraZ = _v; } ); // Vista linking property 'Value' for element 'cameraZ'
          _view.tests.linkProperty("Height",  function() { return heightGraph2; }, function(_v) { heightGraph2 = _v; } ); // Vista linking property 'Height' for element 'tests'
          _view.campoNumericoP.linkProperty("Value",  function() { return P; }, function(_v) { P = _v; } ); // Vista linking property 'Value' for element 'campoNumericoP'
          _view.campoNumericoP2.linkProperty("Value",  function() { return I; }, function(_v) { I = _v; } ); // Vista linking property 'Value' for element 'campoNumericoP2'
          _view.campoNumericoP3.linkProperty("Value",  function() { return D; }, function(_v) { D = _v; } ); // Vista linking property 'Value' for element 'campoNumericoP3'
          _view.campoNumericoP3232.linkProperty("Value",  function() { return PM; }, function(_v) { PM = _v; } ); // Vista linking property 'Value' for element 'campoNumericoP3232'
          _view.campoNumericoP32.linkProperty("Value",  function() { return Ptest; }, function(_v) { Ptest = _v; } ); // Vista linking property 'Value' for element 'campoNumericoP32'
          _view.campoNumericoP322.linkProperty("Value",  function() { return Etest; }, function(_v) { Etest = _v; } ); // Vista linking property 'Value' for element 'campoNumericoP322'
          _view.campoNumericoP3233.linkProperty("Value",  function() { return vG; }, function(_v) { vG = _v; } ); // Vista linking property 'Value' for element 'campoNumericoP3233'
          _view.campoNumericoP323.linkProperty("Value",  function() { return D; }, function(_v) { D = _v; } ); // Vista linking property 'Value' for element 'campoNumericoP323'
          _view.playPauseButton.setAction("OffClick", _pause); // Vista setting action 'OffClick' for element 'playPauseButton'
          _view.playPauseButton.linkProperty("State",  function() { return _isPaused; }, function(_v) { _isPaused = _v; } ); // Vista linking property 'State' for element 'playPauseButton'
          _view.playPauseButton.setAction("OnClick", _play); // Vista setting action 'OnClick' for element 'playPauseButton'
          _view.stepButton.setAction("OnClick", _step); // Vista setting action 'OnClick' for element 'stepButton'
          _view.reloadButton.setAction("OnClick", _reset); // Vista setting action 'OnClick' for element 'reloadButton'
          _view.transparencyCase2.setAction("OnCheckOff", function(_data,_info) {
  acrylicTransparency=255;

}); // Vista setting action 'OnCheckOff' for element 'transparencyCase2'
          _view.transparencyCase2.setAction("OnCheckOn", function(_data,_info) {
  acrylicTransparency=240;

}); // Vista setting action 'OnCheckOn' for element 'transparencyCase2'
          _view.vectorsView.setAction("OnCheckOff", function(_data,_info) {
  vectorsView=false;

}); // Vista setting action 'OnCheckOff' for element 'vectorsView'
          _view.vectorsView.setAction("OnCheckOn", function(_data,_info) {
  vectorsView= true;

}); // Vista setting action 'OnCheckOn' for element 'vectorsView'
          _view.cameraPrincipal.linkProperty("Checked",  function() { return cameraInitial; }, function(_v) { cameraInitial = _v; } ); // Vista linking property 'Checked' for element 'cameraPrincipal'
          _view.cameraPrincipal.setAction("OnCheckOn", function(_data,_info) {
  cameraAltitude= 0;
  cameraAzimuth= 90;
  cameraX= 600;
  cameraY= 0;
  cameraZ= 50;
  zOffset= 175;
  cameraProjection="";
  cameraAbove=false;

}); // Vista setting action 'OnCheckOn' for element 'cameraPrincipal'
          _view.cameraAbove.linkProperty("Checked",  function() { return cameraAbove; }, function(_v) { cameraAbove = _v; } ); // Vista linking property 'Checked' for element 'cameraAbove'
          _view.cameraAbove.setAction("OnCheckOn", function(_data,_info) {
  cameraAltitude= -27;
  cameraAzimuth= 90;
  cameraX= 600;
  cameraY= 0;
  cameraZ= 50;
  zOffset= 210;
  cameraProjection="PERSPECTIVE_OFF";
  cameraInitial=false;
  cameraAbove=true;

}); // Vista setting action 'OnCheckOn' for element 'cameraAbove'
          _view.heightInitialSlider.setAction("OnRelease", function(_data,_info) {
  t=0;

}); // Vista setting action 'OnRelease' for element 'heightInitialSlider'
          _view.heightInitialSlider.linkProperty("Maximum",  function() { return 350/2+25/2; } ); // Vista linking property 'Maximum' for element 'heightInitialSlider'
          _view.heightInitialSlider.linkProperty("Value",  function() { return z0; }, function(_v) { z0 = _v; } ); // Vista linking property 'Value' for element 'heightInitialSlider'
          _view.heightInitialSlider.setAction("OnChange", _pause); // Vista setting action 'OnChange' for element 'heightInitialSlider'
          _view.heightInitialValue.linkProperty("Value",  function() { return 2*z0; } ); // Vista linking property 'Value' for element 'heightInitialValue'
          _view.heightFinalvalue.linkProperty("Value",  function() { return zSP; }, function(_v) { zSP = _v; } ); // Vista linking property 'Value' for element 'heightFinalvalue'
          _view.heightFinalValue.linkProperty("Value",  function() { return zSP+25; } ); // Vista linking property 'Value' for element 'heightFinalValue'
          _view.VinZero.setAction("OnCheckOn", UZero); // Vista setting action 'OnCheckOn' for element 'VinZero'
          _view.Vinslider.setAction("OnRelease", _play); // Vista setting action 'OnRelease' for element 'Vinslider'
          _view.Vinslider.linkProperty("Value",  function() { return U; }, function(_v) { U = _v; } ); // Vista linking property 'Value' for element 'Vinslider'
          _view.Vinslider.setAction("OnChange", _pause); // Vista setting action 'OnChange' for element 'Vinslider'
          _view.VinValue.linkProperty("Value",  function() { return U; }, function(_v) { U = _v; } ); // Vista linking property 'Value' for element 'VinValue'
          _view.time.linkProperty("Value",  function() { return t; }, function(_v) { t = _v; } ); // Vista linking property 'Value' for element 'time'
          _view.PValue.linkProperty("Value",  function() { return Kp; }, function(_v) { Kp = _v; } ); // Vista linking property 'Value' for element 'PValue'
          _view.PZero.setAction("OnCheckOn", PZero); // Vista setting action 'OnCheckOn' for element 'PZero'
          _view.PSlider.linkProperty("Value",  function() { return Kp; }, function(_v) { Kp = _v; } ); // Vista linking property 'Value' for element 'PSlider'
          _view.IValue.linkProperty("Value",  function() { return Ki; }, function(_v) { Ki = _v; } ); // Vista linking property 'Value' for element 'IValue'
          _view.IZero.setAction("OnCheckOn", IZero); // Vista setting action 'OnCheckOn' for element 'IZero'
          _view.ISlider.linkProperty("Value",  function() { return Ki; }, function(_v) { Ki = _v; } ); // Vista linking property 'Value' for element 'ISlider'
          _view.DValue.linkProperty("Value",  function() { return Kd; }, function(_v) { Kd = _v; } ); // Vista linking property 'Value' for element 'DValue'
          _view.DZero.setAction("OnCheckOn", DZero); // Vista setting action 'OnCheckOn' for element 'DZero'
          _view.DSlider.linkProperty("Value",  function() { return Kd; }, function(_v) { Kd = _v; } ); // Vista linking property 'Value' for element 'DSlider'
          break;
      } // end of switch
    }; // end of new reset

    _model.setView(_view);
    _model.reset();
    _view._enableEPub();
  } // end of _selectView

  _model.setAutoplay(false);
  _model.setFPS(20);
  _model.setStepsPerDisplay(20);
  _selectView(-1); // this includes _model.reset()
  return _model;
}
function tfg10_View (_topFrame,_viewNumber,_libraryPath,_codebasePath) {
  var _view;
  switch(_viewNumber) {
    case -10 : break; // make Lint happy
    default :
    case 0: _view = tfg10_View_0 (_topFrame); break;
  } // end of switch

  if (_codebasePath) _view._setResourcePath(_codebasePath);

  if (_libraryPath) _view._setLibraryPath(_libraryPath);


  return _view;
} // end of main function

function tfg10_View_0 (_topFrame) {
  var _view = EJSS_CORE.createView(_topFrame);

  _view._reset = function() {
    _view._clearAll();
    _view._addElement(EJSS_INTERFACE.panel,"header", _view._topFrame) // EJsS HtmlView.Vista: declaration of element 'header'
      .setProperty("ClassName","grayheader") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'header'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"headerText", _view.header) // EJsS HtmlView.Vista: declaration of element 'headerText'
      .setProperty("TextAlign","center") // EJsS HtmlView.Vista: setting property 'TextAlign' for element 'headerText'
      .setProperty("ClassName","Title") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'headerText'
      .setProperty("Text","Simulación Rotor EJSS") // EJsS HtmlView.Vista: setting property 'Text' for element 'headerText'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"main_window", _view._topFrame) // EJsS HtmlView.Vista: declaration of element 'main_window'
      .setProperty("CSS",{"display":"block",   "padding":"0px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'main_window'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"main_left", _view.main_window) // EJsS HtmlView.Vista: declaration of element 'main_left'
      .setProperty("Width","33%") // EJsS HtmlView.Vista: setting property 'Width' for element 'main_left'
      .setProperty("CSS",{"display": "inline-block", "padding-top":"0px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'main_left'
      .setProperty("Display","inline-block") // EJsS HtmlView.Vista: setting property 'Display' for element 'main_left'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"simulacion", _view.main_left) // EJsS HtmlView.Vista: declaration of element 'simulacion'
      .setProperty("ClassName","view3D") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'simulacion'
      .setProperty("Display","inline-block") // EJsS HtmlView.Vista: setting property 'Display' for element 'simulacion'
      ;

    _view._addElement(EJSS_DRAWING3D.drawingPanel,"panelDibujo3D", _view.simulacion) // EJsS HtmlView.Vista: declaration of element 'panelDibujo3D'
      .setProperty("Draggable","ANY") // EJsS HtmlView.Vista: setting property 'Draggable' for element 'panelDibujo3D'
      .setProperty("Enabled",true) // EJsS HtmlView.Vista: setting property 'Enabled' for element 'panelDibujo3D'
      .setProperty("Projection","PERSPECTIVE_ON") // EJsS HtmlView.Vista: setting property 'Projection' for element 'panelDibujo3D'
      .setProperty("DecorationType","NONE") // EJsS HtmlView.Vista: setting property 'DecorationType' for element 'panelDibujo3D'
      .setProperty("Display","block") // EJsS HtmlView.Vista: setting property 'Display' for element 'panelDibujo3D'
      ;

    _view._addElement(EJSS_DRAWING3D.basic,"motor3D", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'motor3D'
      .setProperty("FillColor","rgb(255,128,0,1)") // EJsS HtmlView.Vista: setting property 'FillColor' for element 'motor3D'
      .setProperty("Description","./models/Motor.js") // EJsS HtmlView.Vista: setting property 'Description' for element 'motor3D'
      ;

    _view._addElement(EJSS_DRAWING3D.basic,"platform3D", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'platform3D'
      .setProperty("ColorReflection",0) // EJsS HtmlView.Vista: setting property 'ColorReflection' for element 'platform3D'
      .setProperty("FillColor","black") // EJsS HtmlView.Vista: setting property 'FillColor' for element 'platform3D'
      .setProperty("Description","./models/Platform.js") // EJsS HtmlView.Vista: setting property 'Description' for element 'platform3D'
      .setProperty("Shininess",0) // EJsS HtmlView.Vista: setting property 'Shininess' for element 'platform3D'
      .setProperty("SpecularReflection",0) // EJsS HtmlView.Vista: setting property 'SpecularReflection' for element 'platform3D'
      .setProperty("AmbientReflection",0) // EJsS HtmlView.Vista: setting property 'AmbientReflection' for element 'platform3D'
      ;

    _view._addElement(EJSS_DRAWING3D.basic,"case3D", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'case3D'
      .setProperty("FillColor","Blue") // EJsS HtmlView.Vista: setting property 'FillColor' for element 'case3D'
      .setProperty("Description","./models/Acrylic_Case.js") // EJsS HtmlView.Vista: setting property 'Description' for element 'case3D'
      ;

    _view._addElement(EJSS_DRAWING3D.basic,"caseTop3D", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'caseTop3D'
      .setProperty("FillColor","Blue") // EJsS HtmlView.Vista: setting property 'FillColor' for element 'caseTop3D'
      .setProperty("Description","./models/Acrylic_Case_Top.js") // EJsS HtmlView.Vista: setting property 'Description' for element 'caseTop3D'
      ;

    _view._addElement(EJSS_DRAWING3D.basic,"Dpropeller3D", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'Dpropeller3D'
      .setProperty("FillColor","green") // EJsS HtmlView.Vista: setting property 'FillColor' for element 'Dpropeller3D'
      .setProperty("Description","./models/2Blades_Propeller.js") // EJsS HtmlView.Vista: setting property 'Description' for element 'Dpropeller3D'
      ;

    _view._addElement(EJSS_DRAWING3D.rotationZ,"DpropellerRotation", _view.Dpropeller3D) // EJsS HtmlView.Vista: declaration of element 'DpropellerRotation'
      ;

    _view._addElement(EJSS_DRAWING3D.basic,"Tpropeller3D", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'Tpropeller3D'
      .setProperty("FillColor","green") // EJsS HtmlView.Vista: setting property 'FillColor' for element 'Tpropeller3D'
      .setProperty("Description","./models/3Blades_Propeller.js") // EJsS HtmlView.Vista: setting property 'Description' for element 'Tpropeller3D'
      ;

    _view._addElement(EJSS_DRAWING3D.rotationZ,"TpropellerRotation", _view.Tpropeller3D) // EJsS HtmlView.Vista: declaration of element 'TpropellerRotation'
      ;

    _view._addElement(EJSS_DRAWING3D.basic,"verticalBar3D", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'verticalBar3D'
      .setProperty("FillColor","LightGray") // EJsS HtmlView.Vista: setting property 'FillColor' for element 'verticalBar3D'
      .setProperty("Description","./models/Vertical_Bar.js") // EJsS HtmlView.Vista: setting property 'Description' for element 'verticalBar3D'
      ;

    _view._addElement(EJSS_DRAWING3D.basic,"leftRod3D", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'leftRod3D'
      .setProperty("FillColor","gray") // EJsS HtmlView.Vista: setting property 'FillColor' for element 'leftRod3D'
      .setProperty("Description","./models/LRod.js") // EJsS HtmlView.Vista: setting property 'Description' for element 'leftRod3D'
      ;

    _view._addElement(EJSS_DRAWING3D.basic,"rightRod3D", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'rightRod3D'
      .setProperty("FillColor","gray") // EJsS HtmlView.Vista: setting property 'FillColor' for element 'rightRod3D'
      .setProperty("Description","./models/RRod.js") // EJsS HtmlView.Vista: setting property 'Description' for element 'rightRod3D'
      ;

    _view._addElement(EJSS_DRAWING3D.arrow,"weightVector", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'weightVector'
      .setProperty("FillColor","blue") // EJsS HtmlView.Vista: setting property 'FillColor' for element 'weightVector'
      .setProperty("SizeX",0) // EJsS HtmlView.Vista: setting property 'SizeX' for element 'weightVector'
      .setProperty("LineColor","blue") // EJsS HtmlView.Vista: setting property 'LineColor' for element 'weightVector'
      .setProperty("SizeZ",-50) // EJsS HtmlView.Vista: setting property 'SizeZ' for element 'weightVector'
      .setProperty("SizeY",0) // EJsS HtmlView.Vista: setting property 'SizeY' for element 'weightVector'
      .setProperty("DrawFill",true) // EJsS HtmlView.Vista: setting property 'DrawFill' for element 'weightVector'
      ;

    _view._addElement(EJSS_DRAWING3D.text,"weightLabel", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'weightLabel'
      .setProperty("SizeX",150) // EJsS HtmlView.Vista: setting property 'SizeX' for element 'weightLabel'
      .setProperty("FontColor","blue") // EJsS HtmlView.Vista: setting property 'FontColor' for element 'weightLabel'
      .setProperty("X",0) // EJsS HtmlView.Vista: setting property 'X' for element 'weightLabel'
      .setProperty("Y",0) // EJsS HtmlView.Vista: setting property 'Y' for element 'weightLabel'
      .setProperty("Text","P") // EJsS HtmlView.Vista: setting property 'Text' for element 'weightLabel'
      .setProperty("SizeZ",150) // EJsS HtmlView.Vista: setting property 'SizeZ' for element 'weightLabel'
      .setProperty("SizeY",150) // EJsS HtmlView.Vista: setting property 'SizeY' for element 'weightLabel'
      ;

    _view._addElement(EJSS_DRAWING3D.rotationZ,"weightRotationLabel", _view.weightLabel) // EJsS HtmlView.Vista: declaration of element 'weightRotationLabel'
      .setProperty("Angle",-1.570795) // EJsS HtmlView.Vista: setting property 'Angle' for element 'weightRotationLabel'
      ;

    _view._addElement(EJSS_DRAWING3D.arrow,"thrustVector", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'thrustVector'
      .setProperty("FillColor","red") // EJsS HtmlView.Vista: setting property 'FillColor' for element 'thrustVector'
      .setProperty("SizeX",0) // EJsS HtmlView.Vista: setting property 'SizeX' for element 'thrustVector'
      .setProperty("LineColor","red") // EJsS HtmlView.Vista: setting property 'LineColor' for element 'thrustVector'
      .setProperty("SizeY",0) // EJsS HtmlView.Vista: setting property 'SizeY' for element 'thrustVector'
      ;

    _view._addElement(EJSS_DRAWING3D.text,"thrustLabel", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'thrustLabel'
      .setProperty("SizeX",150) // EJsS HtmlView.Vista: setting property 'SizeX' for element 'thrustLabel'
      .setProperty("FontColor","red") // EJsS HtmlView.Vista: setting property 'FontColor' for element 'thrustLabel'
      .setProperty("Text","E") // EJsS HtmlView.Vista: setting property 'Text' for element 'thrustLabel'
      .setProperty("SizeZ",150) // EJsS HtmlView.Vista: setting property 'SizeZ' for element 'thrustLabel'
      .setProperty("SizeY",150) // EJsS HtmlView.Vista: setting property 'SizeY' for element 'thrustLabel'
      ;

    _view._addElement(EJSS_DRAWING3D.rotationZ,"thrustRotationLabel", _view.thrustLabel) // EJsS HtmlView.Vista: declaration of element 'thrustRotationLabel'
      .setProperty("Angle",-1.570795) // EJsS HtmlView.Vista: setting property 'Angle' for element 'thrustRotationLabel'
      ;

    _view._addElement(EJSS_DRAWING3D.segment,"zDesired", _view.panelDibujo3D) // EJsS HtmlView.Vista: declaration of element 'zDesired'
      .setProperty("SizeX",175) // EJsS HtmlView.Vista: setting property 'SizeX' for element 'zDesired'
      .setProperty("LineColor","red") // EJsS HtmlView.Vista: setting property 'LineColor' for element 'zDesired'
      .setProperty("X",-125) // EJsS HtmlView.Vista: setting property 'X' for element 'zDesired'
      .setProperty("SizeY",175) // EJsS HtmlView.Vista: setting property 'SizeY' for element 'zDesired'
      ;

    _view._addElement(EJSS_DRAWING3D.rotationZ,"zDesiredRotation", _view.zDesired) // EJsS HtmlView.Vista: declaration of element 'zDesiredRotation'
      .setProperty("Angle",-0.7853975) // EJsS HtmlView.Vista: setting property 'Angle' for element 'zDesiredRotation'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"main_right", _view.main_window) // EJsS HtmlView.Vista: declaration of element 'main_right'
      .setProperty("Height",525) // EJsS HtmlView.Vista: setting property 'Height' for element 'main_right'
      .setProperty("Width","67%") // EJsS HtmlView.Vista: setting property 'Width' for element 'main_right'
      .setProperty("Display","inline-block") // EJsS HtmlView.Vista: setting property 'Display' for element 'main_right'
      ;

    _view._addElement(EJSS_INTERFACE.tabbedPanel,"tabsPanel", _view.main_right) // EJsS HtmlView.Vista: declaration of element 'tabsPanel'
      .setProperty("Width","100%") // EJsS HtmlView.Vista: setting property 'Width' for element 'tabsPanel'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"heightTab", _view.tabsPanel) // EJsS HtmlView.Vista: declaration of element 'heightTab'
      .setProperty("ClassName","tabTabbed") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'heightTab'
      ;

    _view._addElement(EJSS_DRAWING2D.plottingPanel,"heightGraph", _view.heightTab) // EJsS HtmlView.Vista: declaration of element 'heightGraph'
      .setProperty("MinimumY",0) // EJsS HtmlView.Vista: setting property 'MinimumY' for element 'heightGraph'
      .setProperty("TitleY","Altura (mm)") // EJsS HtmlView.Vista: setting property 'TitleY' for element 'heightGraph'
      .setProperty("AutoScaleY",true) // EJsS HtmlView.Vista: setting property 'AutoScaleY' for element 'heightGraph'
      .setProperty("TitleX","Tiempo (s)") // EJsS HtmlView.Vista: setting property 'TitleX' for element 'heightGraph'
      .setProperty("AutoScaleX",false) // EJsS HtmlView.Vista: setting property 'AutoScaleX' for element 'heightGraph'
      .setProperty("Title","Gráfica Altura") // EJsS HtmlView.Vista: setting property 'Title' for element 'heightGraph'
      .setProperty("MaximumY",375) // EJsS HtmlView.Vista: setting property 'MaximumY' for element 'heightGraph'
      ;

    _view._addElement(EJSS_DRAWING2D.trail,"heightLine", _view.heightGraph) // EJsS HtmlView.Vista: declaration of element 'heightLine'
      .setProperty("LineColor","blue") // EJsS HtmlView.Vista: setting property 'LineColor' for element 'heightLine'
      .setProperty("NoRepeat",true) // EJsS HtmlView.Vista: setting property 'NoRepeat' for element 'heightLine'
      .setProperty("LineWidth",2) // EJsS HtmlView.Vista: setting property 'LineWidth' for element 'heightLine'
      ;

    _view._addElement(EJSS_DRAWING2D.analyticCurve,"zSetPointLine", _view.heightGraph) // EJsS HtmlView.Vista: declaration of element 'zSetPointLine'
      .setProperty("Minimum",0) // EJsS HtmlView.Vista: setting property 'Minimum' for element 'zSetPointLine'
      .setProperty("LineColor","red") // EJsS HtmlView.Vista: setting property 'LineColor' for element 'zSetPointLine'
      .setProperty("Attributes",{"lineDashType":"dash",}) // EJsS HtmlView.Vista: setting property 'Attributes' for element 'zSetPointLine'
      .setProperty("LineWidth",1.5) // EJsS HtmlView.Vista: setting property 'LineWidth' for element 'zSetPointLine'
      ;

    _view._addElement(EJSS_DRAWING2D.trail,"rastro", _view.heightGraph) // EJsS HtmlView.Vista: declaration of element 'rastro'
      .setProperty("LineColor","Red") // EJsS HtmlView.Vista: setting property 'LineColor' for element 'rastro'
      .setProperty("NoRepeat",true) // EJsS HtmlView.Vista: setting property 'NoRepeat' for element 'rastro'
      ;

    _view._addElement(EJSS_DRAWING2D.trail,"rastro2", _view.heightGraph) // EJsS HtmlView.Vista: declaration of element 'rastro2'
      .setProperty("LineColor","Green") // EJsS HtmlView.Vista: setting property 'LineColor' for element 'rastro2'
      .setProperty("NoRepeat",true) // EJsS HtmlView.Vista: setting property 'NoRepeat' for element 'rastro2'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"voltajeTab", _view.tabsPanel) // EJsS HtmlView.Vista: declaration of element 'voltajeTab'
      .setProperty("ClassName","tabTabbed") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'voltajeTab'
      ;

    _view._addElement(EJSS_DRAWING2D.plottingPanel,"voltajeGraph", _view.voltajeTab) // EJsS HtmlView.Vista: declaration of element 'voltajeGraph'
      .setProperty("EnabledZooming",true) // EJsS HtmlView.Vista: setting property 'EnabledZooming' for element 'voltajeGraph'
      .setProperty("Title","Gráfica Voltaje") // EJsS HtmlView.Vista: setting property 'Title' for element 'voltajeGraph'
      .setProperty("Enabled",true) // EJsS HtmlView.Vista: setting property 'Enabled' for element 'voltajeGraph'
      .setProperty("EnabledDragging","ENABLED_ANY") // EJsS HtmlView.Vista: setting property 'EnabledDragging' for element 'voltajeGraph'
      .setProperty("TitleY","Voltaje (V)") // EJsS HtmlView.Vista: setting property 'TitleY' for element 'voltajeGraph'
      .setProperty("AutoScaleY",true) // EJsS HtmlView.Vista: setting property 'AutoScaleY' for element 'voltajeGraph'
      .setProperty("TitleX","Tiempo (s)") // EJsS HtmlView.Vista: setting property 'TitleX' for element 'voltajeGraph'
      .setProperty("AutoScaleX",false) // EJsS HtmlView.Vista: setting property 'AutoScaleX' for element 'voltajeGraph'
      ;

    _view._addElement(EJSS_DRAWING2D.trail,"voltajeLine", _view.voltajeGraph) // EJsS HtmlView.Vista: declaration of element 'voltajeLine'
      .setProperty("LineColor","red") // EJsS HtmlView.Vista: setting property 'LineColor' for element 'voltajeLine'
      .setProperty("NoRepeat",true) // EJsS HtmlView.Vista: setting property 'NoRepeat' for element 'voltajeLine'
      .setProperty("LineWidth",2) // EJsS HtmlView.Vista: setting property 'LineWidth' for element 'voltajeLine'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"bothTab", _view.tabsPanel) // EJsS HtmlView.Vista: declaration of element 'bothTab'
      .setProperty("ClassName","tabTabbed") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'bothTab'
      ;

    _view._addElement(EJSS_DRAWING2D.plottingPanel,"bothGraph", _view.bothTab) // EJsS HtmlView.Vista: declaration of element 'bothGraph'
      .setProperty("CSS",{"margin":"auto",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'bothGraph'
      .setProperty("TitleY","Altura (mm)/ Voltaje (V)") // EJsS HtmlView.Vista: setting property 'TitleY' for element 'bothGraph'
      .setProperty("AutoScaleY",true) // EJsS HtmlView.Vista: setting property 'AutoScaleY' for element 'bothGraph'
      .setProperty("TitleX","Tiempo (s)") // EJsS HtmlView.Vista: setting property 'TitleX' for element 'bothGraph'
      .setProperty("AutoScaleX",true) // EJsS HtmlView.Vista: setting property 'AutoScaleX' for element 'bothGraph'
      .setProperty("Title","Ambos valores") // EJsS HtmlView.Vista: setting property 'Title' for element 'bothGraph'
      .setProperty("Display","block") // EJsS HtmlView.Vista: setting property 'Display' for element 'bothGraph'
      ;

    _view._addElement(EJSS_DRAWING2D.trail,"heightLine2", _view.bothGraph) // EJsS HtmlView.Vista: declaration of element 'heightLine2'
      .setProperty("LineColor","green") // EJsS HtmlView.Vista: setting property 'LineColor' for element 'heightLine2'
      .setProperty("NoRepeat",true) // EJsS HtmlView.Vista: setting property 'NoRepeat' for element 'heightLine2'
      .setProperty("LineWidth",2) // EJsS HtmlView.Vista: setting property 'LineWidth' for element 'heightLine2'
      ;

    _view._addElement(EJSS_DRAWING2D.trail,"voltajeLine2", _view.bothGraph) // EJsS HtmlView.Vista: declaration of element 'voltajeLine2'
      .setProperty("LineColor","red") // EJsS HtmlView.Vista: setting property 'LineColor' for element 'voltajeLine2'
      .setProperty("NoRepeat",true) // EJsS HtmlView.Vista: setting property 'NoRepeat' for element 'voltajeLine2'
      .setProperty("LineWidth",2) // EJsS HtmlView.Vista: setting property 'LineWidth' for element 'voltajeLine2'
      ;

    _view._addElement(EJSS_DRAWING2D.trail,"currentLine2", _view.bothGraph) // EJsS HtmlView.Vista: declaration of element 'currentLine2'
      .setProperty("LineColor","yellow") // EJsS HtmlView.Vista: setting property 'LineColor' for element 'currentLine2'
      .setProperty("NoRepeat",true) // EJsS HtmlView.Vista: setting property 'NoRepeat' for element 'currentLine2'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"settingsTab", _view.tabsPanel) // EJsS HtmlView.Vista: declaration of element 'settingsTab'
      .setProperty("ClassName","tabTabbed") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'settingsTab'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"settingsMotor", _view.settingsTab) // EJsS HtmlView.Vista: declaration of element 'settingsMotor'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"ajustesMotor", _view.settingsMotor) // EJsS HtmlView.Vista: declaration of element 'ajustesMotor'
      .setProperty("ClassName","settingsTitle") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'ajustesMotor'
      .setProperty("Text","Parámetros motor") // EJsS HtmlView.Vista: setting property 'Text' for element 'ajustesMotor'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"valuesMotor", _view.settingsMotor) // EJsS HtmlView.Vista: declaration of element 'valuesMotor'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'valuesMotor'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"massMotorPanel", _view.valuesMotor) // EJsS HtmlView.Vista: declaration of element 'massMotorPanel'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"massMotor", _view.massMotorPanel) // EJsS HtmlView.Vista: declaration of element 'massMotor'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'massMotor'
      .setProperty("Text","Masa motor= ") // EJsS HtmlView.Vista: setting property 'Text' for element 'massMotor'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"massMotorValue", _view.massMotorPanel) // EJsS HtmlView.Vista: declaration of element 'massMotorValue'
      .setProperty("Format","0.") // EJsS HtmlView.Vista: setting property 'Format' for element 'massMotorValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'massMotorValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'massMotorValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"massMotorUnits", _view.massMotorPanel) // EJsS HtmlView.Vista: declaration of element 'massMotorUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'massMotorUnits'
      .setProperty("Text","g") // EJsS HtmlView.Vista: setting property 'Text' for element 'massMotorUnits'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"currentMotorPanel", _view.valuesMotor) // EJsS HtmlView.Vista: declaration of element 'currentMotorPanel'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"currentMotorText2", _view.currentMotorPanel) // EJsS HtmlView.Vista: declaration of element 'currentMotorText2'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'currentMotorText2'
      .setProperty("Text","Intensidad= ") // EJsS HtmlView.Vista: setting property 'Text' for element 'currentMotorText2'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"currentMotorValue2", _view.currentMotorPanel) // EJsS HtmlView.Vista: declaration of element 'currentMotorValue2'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'currentMotorValue2'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'currentMotorValue2'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'currentMotorValue2'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"currentMotorUnits2", _view.currentMotorPanel) // EJsS HtmlView.Vista: declaration of element 'currentMotorUnits2'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'currentMotorUnits2'
      .setProperty("Text","A") // EJsS HtmlView.Vista: setting property 'Text' for element 'currentMotorUnits2'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"currentMaxMotorText", _view.currentMotorPanel) // EJsS HtmlView.Vista: declaration of element 'currentMaxMotorText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'currentMaxMotorText'
      .setProperty("Text","Máx. Intensidad= ") // EJsS HtmlView.Vista: setting property 'Text' for element 'currentMaxMotorText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"currentMaxMotorValue", _view.currentMotorPanel) // EJsS HtmlView.Vista: declaration of element 'currentMaxMotorValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'currentMaxMotorValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'currentMaxMotorValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'currentMaxMotorValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"currentMaxMotorUnits", _view.currentMotorPanel) // EJsS HtmlView.Vista: declaration of element 'currentMaxMotorUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'currentMaxMotorUnits'
      .setProperty("Text","A") // EJsS HtmlView.Vista: setting property 'Text' for element 'currentMaxMotorUnits'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"otherMotorPanel", _view.valuesMotor) // EJsS HtmlView.Vista: declaration of element 'otherMotorPanel'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"inductanciaText", _view.otherMotorPanel) // EJsS HtmlView.Vista: declaration of element 'inductanciaText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'inductanciaText'
      .setProperty("Text","Inductancia= ") // EJsS HtmlView.Vista: setting property 'Text' for element 'inductanciaText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"inductanciaValue", _view.otherMotorPanel) // EJsS HtmlView.Vista: declaration of element 'inductanciaValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'inductanciaValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'inductanciaValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'inductanciaValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"inductanciaUnits", _view.otherMotorPanel) // EJsS HtmlView.Vista: declaration of element 'inductanciaUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'inductanciaUnits'
      .setProperty("Text","V/(A/s) ") // EJsS HtmlView.Vista: setting property 'Text' for element 'inductanciaUnits'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"resistenceText", _view.otherMotorPanel) // EJsS HtmlView.Vista: declaration of element 'resistenceText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'resistenceText'
      .setProperty("Text","Resistencia= ") // EJsS HtmlView.Vista: setting property 'Text' for element 'resistenceText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"resistanceValue", _view.otherMotorPanel) // EJsS HtmlView.Vista: declaration of element 'resistanceValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'resistanceValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'resistanceValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'resistanceValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"resistanceUnits", _view.otherMotorPanel) // EJsS HtmlView.Vista: declaration of element 'resistanceUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'resistanceUnits'
      .setProperty("Text","Ω") // EJsS HtmlView.Vista: setting property 'Text' for element 'resistanceUnits'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"constantsPanel", _view.valuesMotor) // EJsS HtmlView.Vista: declaration of element 'constantsPanel'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"kvText", _view.constantsPanel) // EJsS HtmlView.Vista: declaration of element 'kvText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'kvText'
      .setProperty("Text","Kv= ") // EJsS HtmlView.Vista: setting property 'Text' for element 'kvText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"kvValue", _view.constantsPanel) // EJsS HtmlView.Vista: declaration of element 'kvValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'kvValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'kvValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"kvUnits", _view.constantsPanel) // EJsS HtmlView.Vista: declaration of element 'kvUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'kvUnits'
      .setProperty("Text","rpm/V") // EJsS HtmlView.Vista: setting property 'Text' for element 'kvUnits'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"keText", _view.constantsPanel) // EJsS HtmlView.Vista: declaration of element 'keText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'keText'
      .setProperty("Text","Ke= ") // EJsS HtmlView.Vista: setting property 'Text' for element 'keText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"keValue", _view.constantsPanel) // EJsS HtmlView.Vista: declaration of element 'keValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'keValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'keValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"keUnits", _view.constantsPanel) // EJsS HtmlView.Vista: declaration of element 'keUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'keUnits'
      .setProperty("Text","N m /A") // EJsS HtmlView.Vista: setting property 'Text' for element 'keUnits'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"efficiencyMotorPanel", _view.valuesMotor) // EJsS HtmlView.Vista: declaration of element 'efficiencyMotorPanel'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"efficiencyMotor", _view.efficiencyMotorPanel) // EJsS HtmlView.Vista: declaration of element 'efficiencyMotor'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'efficiencyMotor'
      .setProperty("Text","Rendimiento (N): ") // EJsS HtmlView.Vista: setting property 'Text' for element 'efficiencyMotor'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"efficiencyMotorValue", _view.efficiencyMotorPanel) // EJsS HtmlView.Vista: declaration of element 'efficiencyMotorValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'efficiencyMotorValue'
      .setProperty("Tooltip","Introduce un valor entre 0 y 1") // EJsS HtmlView.Vista: setting property 'Tooltip' for element 'efficiencyMotorValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'efficiencyMotorValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'efficiencyMotorValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"efficiencyUnits", _view.efficiencyMotorPanel) // EJsS HtmlView.Vista: declaration of element 'efficiencyUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'efficiencyUnits'
      .setProperty("Text","%") // EJsS HtmlView.Vista: setting property 'Text' for element 'efficiencyUnits'
      ;

    _view._addElement(EJSS_INTERFACE.separator,"separador2", _view.settingsTab) // EJsS HtmlView.Vista: declaration of element 'separador2'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"settingsPropeller", _view.settingsTab) // EJsS HtmlView.Vista: declaration of element 'settingsPropeller'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"ajustesHelices", _view.settingsPropeller) // EJsS HtmlView.Vista: declaration of element 'ajustesHelices'
      .setProperty("ClassName","settingsTitle") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'ajustesHelices'
      .setProperty("Text","Parámetros hélices") // EJsS HtmlView.Vista: setting property 'Text' for element 'ajustesHelices'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"valuesPropeller", _view.settingsPropeller) // EJsS HtmlView.Vista: declaration of element 'valuesPropeller'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"propellerSelector", _view.valuesPropeller) // EJsS HtmlView.Vista: declaration of element 'propellerSelector'
      .setProperty("CSS",{"padding": "2px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'propellerSelector'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"DpropellerText", _view.propellerSelector) // EJsS HtmlView.Vista: declaration of element 'DpropellerText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'DpropellerText'
      .setProperty("Text","2 palas:") // EJsS HtmlView.Vista: setting property 'Text' for element 'DpropellerText'
      ;

    _view._addElement(EJSS_INTERFACE.checkBox,"DpropellerSelector", _view.propellerSelector) // EJsS HtmlView.Vista: declaration of element 'DpropellerSelector'
      .setProperty("ClassName","paddedButton") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'DpropellerSelector'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"TpropellerText", _view.propellerSelector) // EJsS HtmlView.Vista: declaration of element 'TpropellerText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'TpropellerText'
      .setProperty("Text","3 palas:") // EJsS HtmlView.Vista: setting property 'Text' for element 'TpropellerText'
      ;

    _view._addElement(EJSS_INTERFACE.checkBox,"TpropellerSelector", _view.propellerSelector) // EJsS HtmlView.Vista: declaration of element 'TpropellerSelector'
      .setProperty("ClassName","paddedButton") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'TpropellerSelector'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"NpropellerText", _view.propellerSelector) // EJsS HtmlView.Vista: declaration of element 'NpropellerText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'NpropellerText'
      .setProperty("Text","Número hélices:  ") // EJsS HtmlView.Vista: setting property 'Text' for element 'NpropellerText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"NpropellerValue", _view.propellerSelector) // EJsS HtmlView.Vista: declaration of element 'NpropellerValue'
      .setProperty("CSS",{"width":"25px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'NpropellerValue'
      .setProperty("Format","0.") // EJsS HtmlView.Vista: setting property 'Format' for element 'NpropellerValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'NpropellerValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'NpropellerValue'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"massPropeller", _view.valuesPropeller) // EJsS HtmlView.Vista: declaration of element 'massPropeller'
      .setProperty("CSS",{"padding": "5px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'massPropeller'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"massPropellerText", _view.massPropeller) // EJsS HtmlView.Vista: declaration of element 'massPropellerText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'massPropellerText'
      .setProperty("Text","Masa hélices= ") // EJsS HtmlView.Vista: setting property 'Text' for element 'massPropellerText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"massPropellerValue", _view.massPropeller) // EJsS HtmlView.Vista: declaration of element 'massPropellerValue'
      .setProperty("Format","0.") // EJsS HtmlView.Vista: setting property 'Format' for element 'massPropellerValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'massPropellerValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'massPropellerValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"massPropellerUnits", _view.massPropeller) // EJsS HtmlView.Vista: declaration of element 'massPropellerUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'massPropellerUnits'
      .setProperty("Text","g") // EJsS HtmlView.Vista: setting property 'Text' for element 'massPropellerUnits'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"diameterPropeller", _view.valuesPropeller) // EJsS HtmlView.Vista: declaration of element 'diameterPropeller'
      .setProperty("CSS",{"padding": "5px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'diameterPropeller'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"diameterPropellerText", _view.diameterPropeller) // EJsS HtmlView.Vista: declaration of element 'diameterPropellerText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'diameterPropellerText'
      .setProperty("Text","Diámetro hélices= ") // EJsS HtmlView.Vista: setting property 'Text' for element 'diameterPropellerText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"diameterPropellerValue", _view.diameterPropeller) // EJsS HtmlView.Vista: declaration of element 'diameterPropellerValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'diameterPropellerValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'diameterPropellerValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'diameterPropellerValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"diameterPropellerUnits", _view.diameterPropeller) // EJsS HtmlView.Vista: declaration of element 'diameterPropellerUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'diameterPropellerUnits'
      .setProperty("Text","cm") // EJsS HtmlView.Vista: setting property 'Text' for element 'diameterPropellerUnits'
      ;

    _view._addElement(EJSS_INTERFACE.separator,"separador", _view.settingsTab) // EJsS HtmlView.Vista: declaration of element 'separador'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"settingsView", _view.settingsTab) // EJsS HtmlView.Vista: declaration of element 'settingsView'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"vista3D", _view.settingsView) // EJsS HtmlView.Vista: declaration of element 'vista3D'
      .setProperty("Tooltip","No es necesario cambiar nada en esta sección") // EJsS HtmlView.Vista: setting property 'Tooltip' for element 'vista3D'
      .setProperty("ClassName","settingsTitle") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'vista3D'
      .setProperty("Text","Ajustes vista 3D") // EJsS HtmlView.Vista: setting property 'Text' for element 'vista3D'
      ;

    _view._addElement(EJSS_INTERFACE.checkBox,"viewDimensions", _view.settingsView) // EJsS HtmlView.Vista: declaration of element 'viewDimensions'
      .setProperty("Text","2D") // EJsS HtmlView.Vista: setting property 'Text' for element 'viewDimensions'
      .setProperty("Visibility",false) // EJsS HtmlView.Vista: setting property 'Visibility' for element 'viewDimensions'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"settingsValues", _view.settingsView) // EJsS HtmlView.Vista: declaration of element 'settingsValues'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"acrylicCase", _view.settingsValues) // EJsS HtmlView.Vista: declaration of element 'acrylicCase'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"transparencyCaseText", _view.acrylicCase) // EJsS HtmlView.Vista: declaration of element 'transparencyCaseText'
      .setProperty("Text","Acrílico: ") // EJsS HtmlView.Vista: setting property 'Text' for element 'transparencyCaseText'
      ;

    _view._addElement(EJSS_INTERFACE.checkBox,"transparencyCase", _view.acrylicCase) // EJsS HtmlView.Vista: declaration of element 'transparencyCase'
      .setProperty("Checked",true) // EJsS HtmlView.Vista: setting property 'Checked' for element 'transparencyCase'
      .setProperty("ClassName","paddedButton") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'transparencyCase'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"cameraDegrees", _view.settingsValues) // EJsS HtmlView.Vista: declaration of element 'cameraDegrees'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"cameraAltitudeText", _view.cameraDegrees) // EJsS HtmlView.Vista: declaration of element 'cameraAltitudeText'
      .setProperty("Text","Altitud:") // EJsS HtmlView.Vista: setting property 'Text' for element 'cameraAltitudeText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"cameraAltitude", _view.cameraDegrees) // EJsS HtmlView.Vista: declaration of element 'cameraAltitude'
      .setProperty("Format","0.") // EJsS HtmlView.Vista: setting property 'Format' for element 'cameraAltitude'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'cameraAltitude'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'cameraAltitude'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"cameraAzimuthText", _view.cameraDegrees) // EJsS HtmlView.Vista: declaration of element 'cameraAzimuthText'
      .setProperty("Text","Azimuth:") // EJsS HtmlView.Vista: setting property 'Text' for element 'cameraAzimuthText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"cameraAzimuth", _view.cameraDegrees) // EJsS HtmlView.Vista: declaration of element 'cameraAzimuth'
      .setProperty("Format","0.") // EJsS HtmlView.Vista: setting property 'Format' for element 'cameraAzimuth'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'cameraAzimuth'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"zOffsetText", _view.cameraDegrees) // EJsS HtmlView.Vista: declaration of element 'zOffsetText'
      .setProperty("Text","Offset Z:") // EJsS HtmlView.Vista: setting property 'Text' for element 'zOffsetText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"zOffset", _view.cameraDegrees) // EJsS HtmlView.Vista: declaration of element 'zOffset'
      .setProperty("Format","0.") // EJsS HtmlView.Vista: setting property 'Format' for element 'zOffset'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'zOffset'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"cameraZooms", _view.settingsValues) // EJsS HtmlView.Vista: declaration of element 'cameraZooms'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"cameraXText", _view.cameraZooms) // EJsS HtmlView.Vista: declaration of element 'cameraXText'
      .setProperty("Text","Cámara X: ") // EJsS HtmlView.Vista: setting property 'Text' for element 'cameraXText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"cameraX", _view.cameraZooms) // EJsS HtmlView.Vista: declaration of element 'cameraX'
      .setProperty("Format","0.") // EJsS HtmlView.Vista: setting property 'Format' for element 'cameraX'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'cameraX'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"cameraYText", _view.cameraZooms) // EJsS HtmlView.Vista: declaration of element 'cameraYText'
      .setProperty("Text","Cámara Y: ") // EJsS HtmlView.Vista: setting property 'Text' for element 'cameraYText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"cameraY", _view.cameraZooms) // EJsS HtmlView.Vista: declaration of element 'cameraY'
      .setProperty("Format","0.") // EJsS HtmlView.Vista: setting property 'Format' for element 'cameraY'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'cameraY'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"cameraZText", _view.cameraZooms) // EJsS HtmlView.Vista: declaration of element 'cameraZText'
      .setProperty("Text","Cámara Z: ") // EJsS HtmlView.Vista: setting property 'Text' for element 'cameraZText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"cameraZ", _view.cameraZooms) // EJsS HtmlView.Vista: declaration of element 'cameraZ'
      .setProperty("Format","0.") // EJsS HtmlView.Vista: setting property 'Format' for element 'cameraZ'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'cameraZ'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"tests", _view.tabsPanel) // EJsS HtmlView.Vista: declaration of element 'tests'
      .setProperty("ClassName","tabTabbed") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'tests'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"panelP", _view.tests) // EJsS HtmlView.Vista: declaration of element 'panelP'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"etiquetaP", _view.panelP) // EJsS HtmlView.Vista: declaration of element 'etiquetaP'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'etiquetaP'
      .setProperty("Text","P") // EJsS HtmlView.Vista: setting property 'Text' for element 'etiquetaP'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"campoNumericoP", _view.panelP) // EJsS HtmlView.Vista: declaration of element 'campoNumericoP'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'campoNumericoP'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"etiquetaP3", _view.panelP) // EJsS HtmlView.Vista: declaration of element 'etiquetaP3'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'etiquetaP3'
      .setProperty("Text","I") // EJsS HtmlView.Vista: setting property 'Text' for element 'etiquetaP3'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"campoNumericoP2", _view.panelP) // EJsS HtmlView.Vista: declaration of element 'campoNumericoP2'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'campoNumericoP2'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"etiquetaP4", _view.panelP) // EJsS HtmlView.Vista: declaration of element 'etiquetaP4'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'etiquetaP4'
      .setProperty("Text","D") // EJsS HtmlView.Vista: setting property 'Text' for element 'etiquetaP4'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"campoNumericoP3", _view.panelP) // EJsS HtmlView.Vista: declaration of element 'campoNumericoP3'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'campoNumericoP3'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"panelP3232", _view.tests) // EJsS HtmlView.Vista: declaration of element 'panelP3232'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"etiquetaP4232", _view.panelP3232) // EJsS HtmlView.Vista: declaration of element 'etiquetaP4232'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'etiquetaP4232'
      .setProperty("Text","Potencia: ") // EJsS HtmlView.Vista: setting property 'Text' for element 'etiquetaP4232'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"campoNumericoP3232", _view.panelP3232) // EJsS HtmlView.Vista: declaration of element 'campoNumericoP3232'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'campoNumericoP3232'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"panelP32", _view.tests) // EJsS HtmlView.Vista: declaration of element 'panelP32'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"etiquetaP42", _view.panelP32) // EJsS HtmlView.Vista: declaration of element 'etiquetaP42'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'etiquetaP42'
      .setProperty("Text","Peso: ") // EJsS HtmlView.Vista: setting property 'Text' for element 'etiquetaP42'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"campoNumericoP32", _view.panelP32) // EJsS HtmlView.Vista: declaration of element 'campoNumericoP32'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'campoNumericoP32'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"etiquetaP422", _view.panelP32) // EJsS HtmlView.Vista: declaration of element 'etiquetaP422'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'etiquetaP422'
      .setProperty("Text","Empuje: ") // EJsS HtmlView.Vista: setting property 'Text' for element 'etiquetaP422'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"campoNumericoP322", _view.panelP32) // EJsS HtmlView.Vista: declaration of element 'campoNumericoP322'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'campoNumericoP322'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"panelP3233", _view.tests) // EJsS HtmlView.Vista: declaration of element 'panelP3233'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"etiquetaP4233", _view.panelP3233) // EJsS HtmlView.Vista: declaration of element 'etiquetaP4233'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'etiquetaP4233'
      .setProperty("Text","Vg") // EJsS HtmlView.Vista: setting property 'Text' for element 'etiquetaP4233'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"campoNumericoP3233", _view.panelP3233) // EJsS HtmlView.Vista: declaration of element 'campoNumericoP3233'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'campoNumericoP3233'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"panelP323", _view.tests) // EJsS HtmlView.Vista: declaration of element 'panelP323'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"etiquetaP423", _view.panelP323) // EJsS HtmlView.Vista: declaration of element 'etiquetaP423'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'etiquetaP423'
      .setProperty("Text","D") // EJsS HtmlView.Vista: setting property 'Text' for element 'etiquetaP423'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"campoNumericoP323", _view.panelP323) // EJsS HtmlView.Vista: declaration of element 'campoNumericoP323'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'campoNumericoP323'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"controlBar", _view._topFrame) // EJsS HtmlView.Vista: declaration of element 'controlBar'
      .setProperty("ClassName","controlPanel") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'controlBar'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"controlLeft", _view.controlBar) // EJsS HtmlView.Vista: declaration of element 'controlLeft'
      .setProperty("Width","33%") // EJsS HtmlView.Vista: setting property 'Width' for element 'controlLeft'
      .setProperty("CSS",{"display": "inline-block"}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'controlLeft'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"controlButtons", _view.controlLeft) // EJsS HtmlView.Vista: declaration of element 'controlButtons'
      .setProperty("CSS",{"padding-bottom": "10px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'controlButtons'
      ;

    _view._addElement(EJSS_INTERFACE.twoStateButton,"playPauseButton", _view.controlButtons) // EJsS HtmlView.Vista: declaration of element 'playPauseButton'
      .setProperty("Tooltip","Iniciar/Parar") // EJsS HtmlView.Vista: setting property 'Tooltip' for element 'playPauseButton'
      .setProperty("ImageOnUrl","/org/opensourcephysics/resources/controls/images/play.gif") // EJsS HtmlView.Vista: setting property 'ImageOnUrl' for element 'playPauseButton'
      .setProperty("ImageOffUrl","/org/opensourcephysics/resources/controls/images/pause.gif") // EJsS HtmlView.Vista: setting property 'ImageOffUrl' for element 'playPauseButton'
      ;

    _view._addElement(EJSS_INTERFACE.button,"stepButton", _view.controlButtons) // EJsS HtmlView.Vista: declaration of element 'stepButton'
      .setProperty("ImageUrl","/org/opensourcephysics/resources/controls/images/stepforward.gif") // EJsS HtmlView.Vista: setting property 'ImageUrl' for element 'stepButton'
      ;

    _view._addElement(EJSS_INTERFACE.button,"reloadButton", _view.controlButtons) // EJsS HtmlView.Vista: declaration of element 'reloadButton'
      .setProperty("ImageUrl","/org/opensourcephysics/resources/controls/images/reset2.gif") // EJsS HtmlView.Vista: setting property 'ImageUrl' for element 'reloadButton'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"acrylicCase2", _view.controlLeft) // EJsS HtmlView.Vista: declaration of element 'acrylicCase2'
      .setProperty("CSS",{"padding": "5px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'acrylicCase2'
      ;

    _view._addElement(EJSS_INTERFACE.checkBox,"transparencyCase2", _view.acrylicCase2) // EJsS HtmlView.Vista: declaration of element 'transparencyCase2'
      .setProperty("Checked",true) // EJsS HtmlView.Vista: setting property 'Checked' for element 'transparencyCase2'
      .setProperty("ClassName","paddedButton") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'transparencyCase2'
      .setProperty("Text","Acrílico") // EJsS HtmlView.Vista: setting property 'Text' for element 'transparencyCase2'
      ;

    _view._addElement(EJSS_INTERFACE.checkBox,"vectorsView", _view.acrylicCase2) // EJsS HtmlView.Vista: declaration of element 'vectorsView'
      .setProperty("Checked",true) // EJsS HtmlView.Vista: setting property 'Checked' for element 'vectorsView'
      .setProperty("ClassName","paddedButton") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'vectorsView'
      .setProperty("Text","Vectores") // EJsS HtmlView.Vista: setting property 'Text' for element 'vectorsView'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"cameraSettings", _view.controlLeft) // EJsS HtmlView.Vista: declaration of element 'cameraSettings'
      ;

    _view._addElement(EJSS_INTERFACE.checkBox,"cameraPrincipal", _view.cameraSettings) // EJsS HtmlView.Vista: declaration of element 'cameraPrincipal'
      .setProperty("Text","Vista Principal") // EJsS HtmlView.Vista: setting property 'Text' for element 'cameraPrincipal'
      ;

    _view._addElement(EJSS_INTERFACE.checkBox,"cameraAbove", _view.cameraSettings) // EJsS HtmlView.Vista: declaration of element 'cameraAbove'
      .setProperty("Text","Camara 2D") // EJsS HtmlView.Vista: setting property 'Text' for element 'cameraAbove'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"controlRight", _view.controlBar) // EJsS HtmlView.Vista: declaration of element 'controlRight'
      .setProperty("Width","67%") // EJsS HtmlView.Vista: setting property 'Width' for element 'controlRight'
      .setProperty("CSS",{"display": "inline-block"}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'controlRight'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"height", _view.controlRight) // EJsS HtmlView.Vista: declaration of element 'height'
      .setProperty("CSS",{"padding": "5px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'height'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"heightInitialText", _view.height) // EJsS HtmlView.Vista: declaration of element 'heightInitialText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'heightInitialText'
      .setProperty("Text","Altura inicial:") // EJsS HtmlView.Vista: setting property 'Text' for element 'heightInitialText'
      ;

    _view._addElement(EJSS_INTERFACE.slider,"heightInitialSlider", _view.height) // EJsS HtmlView.Vista: declaration of element 'heightInitialSlider'
      .setProperty("Minimum",0) // EJsS HtmlView.Vista: setting property 'Minimum' for element 'heightInitialSlider'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'heightInitialSlider'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"heightInitialValue", _view.height) // EJsS HtmlView.Vista: declaration of element 'heightInitialValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'heightInitialValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'heightInitialValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'heightInitialValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"heightInitialUnits", _view.height) // EJsS HtmlView.Vista: declaration of element 'heightInitialUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'heightInitialUnits'
      .setProperty("Text","mm") // EJsS HtmlView.Vista: setting property 'Text' for element 'heightInitialUnits'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"heightFinalText", _view.height) // EJsS HtmlView.Vista: declaration of element 'heightFinalText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'heightFinalText'
      .setProperty("Text","Altura final:") // EJsS HtmlView.Vista: setting property 'Text' for element 'heightFinalText'
      ;

    _view._addElement(EJSS_INTERFACE.slider,"heightFinalvalue", _view.height) // EJsS HtmlView.Vista: declaration of element 'heightFinalvalue'
      .setProperty("Minimum",-50) // EJsS HtmlView.Vista: setting property 'Minimum' for element 'heightFinalvalue'
      .setProperty("Maximum",350) // EJsS HtmlView.Vista: setting property 'Maximum' for element 'heightFinalvalue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'heightFinalvalue'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"heightFinalValue", _view.height) // EJsS HtmlView.Vista: declaration of element 'heightFinalValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'heightFinalValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'heightFinalValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'heightFinalValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"heightFinalUnits", _view.height) // EJsS HtmlView.Vista: declaration of element 'heightFinalUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'heightFinalUnits'
      .setProperty("Text","mm") // EJsS HtmlView.Vista: setting property 'Text' for element 'heightFinalUnits'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"blank", _view.controlRight) // EJsS HtmlView.Vista: declaration of element 'blank'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"voltaje", _view.controlRight) // EJsS HtmlView.Vista: declaration of element 'voltaje'
      .setProperty("CSS",{"padding": "5px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'voltaje'
      .setProperty("Display","block") // EJsS HtmlView.Vista: setting property 'Display' for element 'voltaje'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"VinText", _view.voltaje) // EJsS HtmlView.Vista: declaration of element 'VinText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'VinText'
      .setProperty("Text","Voltaje: ") // EJsS HtmlView.Vista: setting property 'Text' for element 'VinText'
      ;

    _view._addElement(EJSS_INTERFACE.radioButton,"VinZero", _view.voltaje) // EJsS HtmlView.Vista: declaration of element 'VinZero'
      .setProperty("Checked",false) // EJsS HtmlView.Vista: setting property 'Checked' for element 'VinZero'
      .setProperty("CSS",{"padding":"0px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'VinZero'
      ;

    _view._addElement(EJSS_INTERFACE.slider,"Vinslider", _view.voltaje) // EJsS HtmlView.Vista: declaration of element 'Vinslider'
      .setProperty("Minimum",0) // EJsS HtmlView.Vista: setting property 'Minimum' for element 'Vinslider'
      .setProperty("Maximum",12) // EJsS HtmlView.Vista: setting property 'Maximum' for element 'Vinslider'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'Vinslider'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"VinValue", _view.voltaje) // EJsS HtmlView.Vista: declaration of element 'VinValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'VinValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'VinValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'VinValue'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"VinUnits", _view.voltaje) // EJsS HtmlView.Vista: declaration of element 'VinUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'VinUnits'
      .setProperty("Text"," V") // EJsS HtmlView.Vista: setting property 'Text' for element 'VinUnits'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"timeText", _view.voltaje) // EJsS HtmlView.Vista: declaration of element 'timeText'
      .setProperty("CSS",{"padding-left":"20px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'timeText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'timeText'
      .setProperty("Text","t= ") // EJsS HtmlView.Vista: setting property 'Text' for element 'timeText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"time", _view.voltaje) // EJsS HtmlView.Vista: declaration of element 'time'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'time'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'time'
      .setProperty("Editable",false) // EJsS HtmlView.Vista: setting property 'Editable' for element 'time'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"timeUnits", _view.voltaje) // EJsS HtmlView.Vista: declaration of element 'timeUnits'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'timeUnits'
      .setProperty("Text","s") // EJsS HtmlView.Vista: setting property 'Text' for element 'timeUnits'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"PID", _view.controlRight) // EJsS HtmlView.Vista: declaration of element 'PID'
      .setProperty("CSS",{"padding-top":"10px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'PID'
      .setProperty("Display","block") // EJsS HtmlView.Vista: setting property 'Display' for element 'PID'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"Kp", _view.PID) // EJsS HtmlView.Vista: declaration of element 'Kp'
      .setProperty("ClassName","PIDcol") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'Kp'
      .setProperty("Display","inline-block") // EJsS HtmlView.Vista: setting property 'Display' for element 'Kp'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"PText", _view.Kp) // EJsS HtmlView.Vista: declaration of element 'PText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'PText'
      .setProperty("Text","Kp: ") // EJsS HtmlView.Vista: setting property 'Text' for element 'PText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"PValue", _view.Kp) // EJsS HtmlView.Vista: declaration of element 'PValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'PValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'PValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'PValue'
      ;

    _view._addElement(EJSS_INTERFACE.radioButton,"PZero", _view.Kp) // EJsS HtmlView.Vista: declaration of element 'PZero'
      .setProperty("Checked",false) // EJsS HtmlView.Vista: setting property 'Checked' for element 'PZero'
      .setProperty("CSS",{"padding":"0px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'PZero'
      ;

    _view._addElement(EJSS_INTERFACE.slider,"PSlider", _view.Kp) // EJsS HtmlView.Vista: declaration of element 'PSlider'
      .setProperty("Minimum",0) // EJsS HtmlView.Vista: setting property 'Minimum' for element 'PSlider'
      .setProperty("Maximum",10) // EJsS HtmlView.Vista: setting property 'Maximum' for element 'PSlider'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"Ki", _view.PID) // EJsS HtmlView.Vista: declaration of element 'Ki'
      .setProperty("ClassName","PIDcol") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'Ki'
      .setProperty("Display","inline-block") // EJsS HtmlView.Vista: setting property 'Display' for element 'Ki'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"IText", _view.Ki) // EJsS HtmlView.Vista: declaration of element 'IText'
      .setProperty("CSS",{"padding-left":"15px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'IText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'IText'
      .setProperty("Text","Ki: ") // EJsS HtmlView.Vista: setting property 'Text' for element 'IText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"IValue", _view.Ki) // EJsS HtmlView.Vista: declaration of element 'IValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'IValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'IValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'IValue'
      ;

    _view._addElement(EJSS_INTERFACE.radioButton,"IZero", _view.Ki) // EJsS HtmlView.Vista: declaration of element 'IZero'
      .setProperty("CSS",{"padding":"0px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'IZero'
      ;

    _view._addElement(EJSS_INTERFACE.slider,"ISlider", _view.Ki) // EJsS HtmlView.Vista: declaration of element 'ISlider'
      .setProperty("Minimum",0) // EJsS HtmlView.Vista: setting property 'Minimum' for element 'ISlider'
      .setProperty("Maximum",10) // EJsS HtmlView.Vista: setting property 'Maximum' for element 'ISlider'
      ;

    _view._addElement(EJSS_INTERFACE.panel,"Kd", _view.PID) // EJsS HtmlView.Vista: declaration of element 'Kd'
      .setProperty("ClassName","PIDcol") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'Kd'
      .setProperty("Display","inline-block") // EJsS HtmlView.Vista: setting property 'Display' for element 'Kd'
      ;

    _view._addElement(EJSS_INTERFACE.imageAndTextButton,"DText", _view.Kd) // EJsS HtmlView.Vista: declaration of element 'DText'
      .setProperty("ClassName","textWhite") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'DText'
      .setProperty("Text","Kd: ") // EJsS HtmlView.Vista: setting property 'Text' for element 'DText'
      ;

    _view._addElement(EJSS_INTERFACE.numberField,"DValue", _view.Kd) // EJsS HtmlView.Vista: declaration of element 'DValue'
      .setProperty("Format","0.00") // EJsS HtmlView.Vista: setting property 'Format' for element 'DValue'
      .setProperty("ClassName","whiteValue") // EJsS HtmlView.Vista: setting property 'ClassName' for element 'DValue'
      .setProperty("Editable",true) // EJsS HtmlView.Vista: setting property 'Editable' for element 'DValue'
      ;

    _view._addElement(EJSS_INTERFACE.radioButton,"DZero", _view.Kd) // EJsS HtmlView.Vista: declaration of element 'DZero'
      .setProperty("CSS",{"padding":"0px",}) // EJsS HtmlView.Vista: setting property 'CSS' for element 'DZero'
      ;

    _view._addElement(EJSS_INTERFACE.slider,"DSlider", _view.Kd) // EJsS HtmlView.Vista: declaration of element 'DSlider'
      .setProperty("Minimum",0) // EJsS HtmlView.Vista: setting property 'Minimum' for element 'DSlider'
      .setProperty("Maximum",10) // EJsS HtmlView.Vista: setting property 'Maximum' for element 'DSlider'
      ;

  };

  return _view;
}



      var _model;
      var _scorm;
      window.addEventListener('load',
        function () { 
          _model =  new tfg10("_topFrame","file:/E:/Angel/Mega/Uni/Fisica/TFG/JavaScript_EJS_6.0/bin/javascript/lib/","file:/E:/Angel/Mega/Uni/Fisica/TFG/JavaScript_EJS_6.0/workspace/source/");
          if (typeof _isApp !== "undefined" && _isApp) _model.setRunAlways(true);
          TextResizeDetector.TARGET_ELEMENT_ID = '_topFrame';
          TextResizeDetector.USER_INIT_FUNC = function () {
            var iBase = TextResizeDetector.addEventListener(function(e,args) {
              _model._fontResized(args[0].iBase,args[0].iSize,args[0].iDelta);
              },null);
            _model._fontResized(iBase);
          };
          _model.onload();
        }, false);
