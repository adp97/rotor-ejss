""" This script is intended just for adding custom presets to EJSS autocompiled files """ 
__author__="Angel del Pino Jimenez"

import os

########################################################## CSS  FILE  ##########################################################
filecss='workspace/output/_ejs_library/css/ejss.css' #Define the css file

# Read the file 'ejss.css'
with open(filecss, 'r') as file :
  filedata = file.read()

textcssA="/* This defines styles and classes used for OSP-EjsS pages */"
textcssB= "/* Import custom css file */ \n @import url('custom.css');\n \n /* This defines styles and classes used for OSP-EjsS pages */"
# Search and replace the target text
filedata = filedata.replace(textcssA, textcssB)

# Write the file out again in a new file. Replaces original one
with open(filecss, 'w') as file:
  file.write(filedata)

textcssC='userSelect: none; display: inline-block; color: black; text-align: center;'
textcssD=' ' 

# Search and replace the second target text
filedata = filedata.replace(textcssC, textcssD)

# Write the file out again in a new file. Replaces original one
with open(filecss, 'w') as file:
  file.write(filedata)

textcssE=' padding: 7px 8px; text-decoration: none; transition: 0.3s;  font-size: 14px; '
textcssF=' ' 

# Search and replace the second target text
filedata = filedata.replace(textcssE, textcssF)

# Write the file out again in a new file. Replaces original one
with open(filecss, 'w') as file:
  file.write(filedata)


########################################################## HTML FILE ##########################################################
filehtml='workspace/output/tfg20.xhtml' #Define the HTML file


# Read the file 'tfg10_pid.xhtml'
with open(filehtml, 'r') as file :
  filedata = file.read()

texthtmlC='/css/ejss.css" />'
texthtmlD='/css/ejss.css" /> \n    <link rel="icon" href="file://E:/Angel/Mega/Uni/Fisica/TFG/JavaScript_EJS_6.0/workspace/source/img/icon.ico"/>' 

# Search and replace the second target text
filedata = filedata.replace(texthtmlC, texthtmlD)

# Write the file out again in a new file. Replaces original one
with open(filehtml, 'w') as file:
  file.write(filedata)